package common

const (
	// APPNAME the name of the service
	APPNAME string = "account-service"

	// VAULTPATH the vault path for configuration
	VAULTPATH string = "/vault/secrets"
)

// Purpose
const (
	Verification string = "V"
	Login        string = "L"
	Registration string = "R"
)

// MediaEmail const of media Email
const MediaEmail = "email"

// MediaPhone const of media Phone
const MediaPhone = "phone"

// JWTValidityLengthInHours the length of JWT in hours
const JWTValidityLengthInHours = 15 * 24

// TestingPhoneNumberPrefix those phoneNumbers will have otp 333333
const TestingPhoneNumberPrefix = "8524444"

// DefaultSettings default app settings
const DefaultSettings = `{
        "night_mode": false,
        "pause_all_notif": false,
        "group_msg": true,
        "friend_req": true,
        "bday_notif": true,
        "in_app_preview": true,
        "in_app_sounds": true,
        "in_app_vibrants": true,
        "last_seen": true,
        "profile_photo": true,
        "show_location": true,
        "read_receipts": true,
        "location_svc": "Allow while using"
    }`
