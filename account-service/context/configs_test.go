package context

import (
	"testing"
)

func TestGetConfigValues(t *testing.T) {

	Configuration.Init()
	Configuration.configs["VAULT_PATH"] = "../"

	value := Configuration.Get("UNITTEST_KEY")

	if value != "UNITTEST_VALUE" {
		t.Errorf("Test failed on configs")
	}
}
