package context

import (
	"database/sql"
	"fmt"
	"log"

	migrate "github.com/rubenv/sql-migrate"

	// For mysql connection
	_ "github.com/go-sql-driver/mysql"
)

// Database struct for database
type Database struct {
	connection *sql.DB
}

// DB Database connection
var DB = Database{}

// Migrate run database migrations
func (database *Database) Migrate() {
	migrations := &migrate.FileMigrationSource{
		Dir: fmt.Sprintf("%s/%s", Configuration.Get("APP_DIR"), "db/migrations/mysql"),
	}

	dbConnection := database.GetDBConnection()
	n, err := migrate.Exec(dbConnection, "mysql", migrations, migrate.Up)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Printf("Applied %d migrations!\n", n)
}

// InitDBConn initialize database connection
func (database *Database) InitDBConn() {
	if database.connection == nil {
		database.connection = database.ConnectDatabase()
	}

	err := database.connection.Ping()
	if err != nil {
		log.Fatal("Error on connection to database:", err)
	}
	log.Print("Success in connecting to database")
}

// GetDBConnection get database connection object
func (database *Database) GetDBConnection() *sql.DB {
	if database.connection == nil {
		database.connection = database.ConnectDatabase()
	}

	return database.connection
}

// ConnectDatabase return database connection object
func (database *Database) ConnectDatabase() *sql.DB {
	dbHost, dbName, dbPassword, dbUser := database.getDbConfigs()
	connectionStr := fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8&parseTime=True", dbUser, dbPassword, dbHost, dbName)
	mysqlCon, err := sql.Open("mysql", connectionStr)
	if err != nil {
		log.Fatal(err)
	}

	return mysqlCon
}

func (database *Database) getDbConfigs() (dbHost, dbName, dbPassword, dbUser string) {
	dbHost = Configuration.Get("MYSQL_HOST")
	dbName = Configuration.Get("MYSQL_DBNAME")
	dbPassword = Configuration.Get("MYSQL_PASSWORD")
	dbUser = Configuration.Get("MYSQL_USER")

	return
}
