package controllers

import (
	"log"
	"net/http"
	"regexp"

	"github.com/gin-gonic/gin"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/services"
)

// ConnectionController the endpoints for connnection
type ConnectionController struct {
	services.ConnectionService
}

// RegisterConnectionApis register connection apis to router
func RegisterConnectionApis(router *gin.RouterGroup) {
	api := ConnectionController{}
	router.POST("/add", api.Add)
	router.POST("/search", api.Search)
	router.GET("/:uuid", api.List)
}

// Add create Connection relationships
// @Tags 3-Connection
// @Summary Add connnections
// @Security securitydefinitions.oauth2.application
// @Produce  json
// @Param body body dto.ConnectionRequest true "List of parameters"
// @Success 200 {string} json "{"success":true}"
// @Router /connection/add [post]
func (connectionController *ConnectionController) Add(context *gin.Context) {
	connectionRequest := dto.ConnectionRequest{}
	err := context.BindJSON(&connectionRequest)
	if err != nil {
		log.Print("[ConnectionController.Add] Error binding json in request body: " + err.Error())
		return
	}

	connectionController.ConnectionService.AddConnection(&connectionRequest)
	context.JSON(http.StatusOK, gin.H{"success": true})
}

// Search by phone numbers
// @Summary Search users
// @Security securitydefinitions.oauth2.application
// @Tags 3-Connection
// @Description search by phone numbers
// @Produce json
// @Param body body dto.ContactSearchRequest true "List of parameters"
// @Success 200 {object} dto.ContactSearchResponse
// @Router /connection/search [post]
func (connectionController *ConnectionController) Search(context *gin.Context) {
	request := dto.ContactSearchRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[ConnectionController.Search] Error binding json in request body: " + err.Error())
		return
	}
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	for index := 0; index < len(request.PhoneNumbers); index++ {
		request.PhoneNumbers[index] = reg.ReplaceAllString(request.PhoneNumbers[index], "")
	}

	context.JSON(http.StatusOK, connectionController.ConnectionService.SearchByContacts(&request))
}

// List get all connection profiles
// @Tags 3-Connection
// @Summary List connnections
// @Security securitydefinitions.oauth2.application
// @Produce json
// @Param uuid path string true "uuid"
// @Success 200 {object} dto.ConnectionResponse
// @Router /connection/{uuid} [get]
func (connectionController *ConnectionController) List(context *gin.Context) {
	uuid := context.Param("uuid")
	context.JSON(http.StatusOK, connectionController.ConnectionService.ListConnections(uuid))
}
