package controllers

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvlidConnectionRequest(t *testing.T) {
	router := setupRoute()

	recorder := httptest.NewRecorder()
	jsonStr := []byte(`invalid request`)
	req, _ := http.NewRequest("POST", "/account/v1/connection/add", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)

	assert.Equal(t, 400, recorder.Code)

	req, _ = http.NewRequest("POST", "/account/v1/connection/search", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)
	assert.Equal(t, 400, recorder.Code)
}

func TestAddConnectionRequest(t *testing.T) {
	router := setupRoute()
	user1UUID, user2UUID := setUpData()

	// Test add connection
	addConnectionStr := fmt.Sprintf(
		`{
			"uuid1": "%s",
			"uuid2": "%s",
      "relation": "friends"
		}`,
		user1UUID,
		user2UUID,
	)

	jsonStr := []byte(addConnectionStr)
	req, _ := http.NewRequest("POST", "/account/v1/connection/add", bytes.NewBuffer(jsonStr))
	recorderForAddConnection := httptest.NewRecorder()
	router.ServeHTTP(recorderForAddConnection, req)

	assert.Equal(t, 200, recorderForAddConnection.Code)
	assert.Contains(t, recorderForAddConnection.Body.String(), "success")

	tearDown()
}

func TestSearchConnectionRequest(t *testing.T) {
	router := setupRoute()

	user1UUID, user2UUID := setUpData()

	// Test search connection
	searchStr := fmt.Sprintf(
		`{
			"contacts": [
			"852 44448002",
			"852 44448003"
			],
			"uuid": "%s"
		}`,
		user1UUID,
	)

	jsonStr := []byte(searchStr)
	req, _ := http.NewRequest("POST", "/account/v1/connection/search", bytes.NewBuffer(jsonStr))
	recorderForSearch := httptest.NewRecorder()
	router.ServeHTTP(recorderForSearch, req)

	assert.Equal(t, 200, recorderForSearch.Code)
	assert.Contains(t, recorderForSearch.Body.String(), user2UUID)
	assert.Contains(t, recorderForSearch.Body.String(), "UnConnected")
	assert.Contains(t, recorderForSearch.Body.String(), "UnRegistered")
	assert.Contains(t, recorderForSearch.Body.String(), "Connected")
	assert.Contains(t, recorderForSearch.Body.String(), "85244448003")

	tearDown()
}

func TestListConnectionRequest(t *testing.T) {
	router := setupRoute()
	user1UUID, user2UUID := setUpData()

	// Test list connection endpoint
	recorderForConnection := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/account/v1/connection/"+user1UUID, nil)
	router.ServeHTTP(recorderForConnection, req)

	assert.Equal(t, 200, recorderForConnection.Code)

	assert.Contains(t, recorderForConnection.Body.String(), `"Connections"`)
	assert.Contains(t, recorderForConnection.Body.String(), `"uuid":"`+user2UUID)
	assert.Contains(t, recorderForConnection.Body.String(), `"fullname":"Test User2"`)
	assert.Contains(t, recorderForConnection.Body.String(), `"dateOfBirth":"1990-09-02T00:00:00Z"`)
	assert.Contains(t, recorderForConnection.Body.String(), `"locale":""`)
	assert.Contains(t, recorderForConnection.Body.String(), `"avatarUrl":""`)

	tearDown()
}
