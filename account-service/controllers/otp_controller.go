package controllers

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/common"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/helpers"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/services"
)

// OtpController the endpoints for otp
type OtpController struct {
}

// RegisterOtpApis register otp apis to router
func RegisterOtpApis(router *gin.RouterGroup) {
	api := OtpController{}
	router.POST("/requestOtp", api.RequestOtp)
	router.POST("/verifyOtp", api.VerifyOtp)
}

// RequestOtp request one-time-password
// @Summary Request OTP to be sent via sms/email
// @Tags 1-OneTimePassword
// @Produce  json
// @Param body body dto.OtpRequest true "List of parameters"
// @Success 200 {string} json "{"success":true, "phaseOneToken":"phase one token"}"
// @Router /requestOtp [post]
func (otpController *OtpController) RequestOtp(context *gin.Context) {
	request := dto.OtpRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[Onboarding.RequestOtp] Error binding json in request body: ", err)
		return
	}

	token, err := otpController.handleRequestOtpRequest(&request)
	if err != nil {
		log.Print("[Onboarding] Error generating Phase 1 Token: ", err)

		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"phaseOneToken": token,
	})
	return
}

// VerifyOtp verify otp
// @Summary Verify OTP received via sms/email
// @Tags 1-OneTimePassword
// @Produce json
// @Param body body dto.VerifyOtpRequest true "List of parameters"
// @Success 200 {string} json "{"phaseTwoToken":string}"
// @Router /verifyOtp [post]
func (otpController *OtpController) VerifyOtp(context *gin.Context) {
	request := dto.VerifyOtpRequest{}
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	token, err := otpController.handleVerifyOtpRequest(&request)
	if err != nil {
		log.Print("[Onboarding.VerifyOtp] Error generating Phase 2 Token: " + err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"phaseTwoToken": token,
	})
	return
}

func (otpController *OtpController) handleRequestOtpRequest(request *dto.OtpRequest) (string, error) {
	entity := request.PhoneNumber.FormattedValue()
	_, err := services.FindUserUuidByPhone(entity)

	if request.Purpose == common.Registration && err == nil {
		return "", errors.New("User existed")
	}
	if request.Purpose == common.Login && err != nil {
		return "", errors.New("Please register first")
	}

	return otpController.genPhase1Token(common.MediaPhone, entity)

}

func (otpController *OtpController) handleVerifyOtpRequest(request *dto.VerifyOtpRequest) (string, error) {
	phaseOneToken := request.PhaseOneToken
	code := request.Code
	entity := request.PhoneNumber.FormattedValue()
	purpose := request.Purpose

	err := services.FindValidTokenThenInvalidate(phaseOneToken, code, "V", common.MediaPhone, entity)
	if err != nil {
		return "", err
	}

	return otpController.genPhase2Token(purpose, common.MediaPhone, entity)
}

func (otpController *OtpController) genPhase1Token(media, entity string) (string, error) {
	code := helpers.RandomDigits(6)
	if strings.HasPrefix(entity, common.TestingPhoneNumberPrefix) {
		code = "333333"
	}
	verToken := models.NewToken(code, "V", media, entity)
	token, err := services.SaveToken(&verToken)
	if err != nil {
		return "", err
	}

	if !strings.HasPrefix(entity, common.TestingPhoneNumberPrefix) {
		err = services.AddOtpQueue(token.Entity, token.Media, token.Code)
		if err != nil {
			return "", err
		}
	}

	return token.Token, err
}

func (otpController *OtpController) genPhase2Token(purpose, media, entity string) (string, error) {
	regToken := models.NewToken("", purpose, media, entity)
	token, err := services.SaveToken(&regToken)
	if err != nil {
		return "", err
	}

	return token.Token, err
}
