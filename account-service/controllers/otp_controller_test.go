package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/repositories"
)

func TestInvlidRequest(t *testing.T) {
	router := setupRoute()

	recorder := httptest.NewRecorder()
	jsonStr := []byte(`invalid request`)
	req, _ := http.NewRequest("POST", "/account/v1/requestOtp", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)

	assert.Equal(t, 400, recorder.Code)

	req, _ = http.NewRequest("POST", "/account/v1/verifyOtp", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)
	assert.Equal(t, 400, recorder.Code)
	tearDown()
}

func TestRegister(t *testing.T) {
	router := setupRoute()

	// Test requestOtp endpoint
	recorderForRequestOtp := httptest.NewRecorder()
	jsonStr := []byte(`{"areaCode": "852","phone": "44444321","purpose": "R"}`)
	req, _ := http.NewRequest("POST", "/account/v1/requestOtp", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorderForRequestOtp, req)

	assert.Equal(t, 200, recorderForRequestOtp.Code)
	assert.Contains(t, recorderForRequestOtp.Body.String(), "phaseOneToken")

	// Get the phaseOneToken from last endpoint
	token := make(map[string]string)
	json.Unmarshal(recorderForRequestOtp.Body.Bytes(), &token)

	// Test verifyOtp endpoint
	verifyRequestStr := fmt.Sprintf(
		`{
			"areaCode": "%s",
			"code": "%s",
			"phaseOneToken": "%s",
			"phone": "%s",
			"purpose": "R"
		}`,
		"852",
		"333333",
		token["phaseOneToken"],
		"44444321",
	)
	jsonStr = []byte(verifyRequestStr)
	req, _ = http.NewRequest("POST", "/account/v1/verifyOtp", bytes.NewBuffer(jsonStr))
	recorderForVerifyOtp := httptest.NewRecorder()
	router.ServeHTTP(recorderForVerifyOtp, req)

	assert.Equal(t, 200, recorderForVerifyOtp.Code)
	assert.Contains(t, recorderForVerifyOtp.Body.String(), "phaseTwoToken")

	// Get the phaseTwoToken from last endpoint
	json.Unmarshal(recorderForVerifyOtp.Body.Bytes(), &token)

	// Testing Register endpoint
	registerRequestStr := fmt.Sprintf(
		`{
			"areaCode": "%s",
  		"dateOfBirth": "%s",
  		"fullname": "%s",
  		"phaseTwoToken": "%s",
  		"phone": "%s"
		}`,
		"852",
		"1990-09-01",
		"Controller Testing",
		token["phaseTwoToken"],
		"44444321",
	)
	jsonStr = []byte(registerRequestStr)
	req, _ = http.NewRequest("POST", "/account/v1/register", bytes.NewBuffer(jsonStr))
	recorderForRegister := httptest.NewRecorder()
	router.ServeHTTP(recorderForRegister, req)

	assert.Equal(t, 200, recorderForRegister.Code)
	assert.Contains(t, recorderForRegister.Body.String(), "uuid")
	assert.Contains(t, recorderForRegister.Body.String(), "jwt")

	// Delete This user
	repository := repositories.UserRepository{}
	repository.DeleteByPhoneNumber("85244444321")
}
