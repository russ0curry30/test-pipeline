package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/services"
)

// SettingsController the endpoints for users
type SettingsController struct {
}

// Settings settings struct
type Settings struct {
	NightMode     bool   `json:"night_mode"`
	PauseAllNotif bool   `json:"pause_all_notif"`
	GroupMsg      bool   `json:"group_msg"`
	FriendReq     bool   `json:"friend_req"`
	BdayNotif     bool   `json:"bday_notif"`
	Preview       bool   `json:"in_app_preview"`
	Sounds        bool   `json:"in_app_sounds"`
	Vibrants      bool   `json:"in_app_vibrants"`
	LastSeen      bool   `json:"last_seen"`
	Photo         bool   `json:"profile_photo"`
	Location      bool   `json:"show_location"`
	Receipt       bool   `json:"read_receipts"`
	LocationSvc   string `json:"location_svc"`
}

// SettingsApis register user apis to router
func SettingsApis(router *gin.RouterGroup) {
	api := SettingsController{}
	router.GET("/:uuid", api.GetSettings)
	router.POST("/", api.UpdateSettings)
}

// GetSettings Get user settings
// @Summary User settings
// @Security securitydefinitions.oauth2.application
// @Tags 4-Settings
// @Produce json
// @Param uuid path string true "uuid"
// @Success 200 {object}  dto.SettingsResponse
// @Router /settings/{uuid} [get]
func (SettingsController *SettingsController) GetSettings(context *gin.Context) {
	uuid := context.Param("uuid")

	settingsData, err := services.GetSettingsByUuid(uuid)
	if err != nil {
		log.Printf("[SettingsController.GetSettings] Error fetching user settings: %s for %s:", uuid, err.Error())
	}

	settings := Settings{}
	json.Unmarshal(settingsData, &settings)
	context.JSON(http.StatusOK, gin.H{
		"settings": settings,
	})
}

// UpdateSettings Update user settings
// @Summary User settings
// @Security securitydefinitions.oauth2.application
// @Tags 4-Settings
// @Produce json
// @Param body body dto.UpdateSettingsRequest true "Settings: {} => can be one or more fields"
// @Success 200 {string} json "{"success":true}"
// @Router /settings [post]
func (SettingsController *SettingsController) UpdateSettings(context *gin.Context) {
	request := dto.UpdateSettingsRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[SettingsController.UpdateSettings] Error decoding json in request body: " + err.Error())
		return
	}

	uuid := request.UUID
	settings := request.Settings

	var b []byte
	b, err = json.Marshal(&settings)
	if err != nil {
		log.Printf("[SettingsController.UpdateSettings] Error encoding user settings: %s for %s:", uuid, err.Error())
		return
	}

	err = services.UpdateSettingByUuid(uuid, b)

	if err != nil {
		log.Printf("[SettingsController.UpdateSettings] Error updating user settings: %s for %s:", uuid, err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{"success": true})

}
