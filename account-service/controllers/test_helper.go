package controllers

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/repositories"
)

func setupTestEnv() {
	if os.Getenv("ENV") == "local" || os.Getenv("ENV") == "" || os.Getenv("ENV") == "unittest" {
		os.Setenv("ENV", "unittest")
	} else {
		os.Setenv("ENV", "ci")
	}
}

func setupRoute() *gin.Engine {
	setupTestEnv()
	context.Configuration.Init()
	context.DB.Migrate()
	router := gin.Default()

	apiGroup := router.Group("/account/v1")
	RegisterOtpApis(apiGroup)
	RegisterUserApis(apiGroup)
	connectionGroup := apiGroup.Group("/connection")
	RegisterConnectionApis(connectionGroup)
	return router
}

func setUpData() (string, string) {
	userRepository := repositories.UserRepository{}
	connectionRepository := repositories.ConnectionRepository{}

	// Create 2 test users
	user1 := models.User{
		Fullname:    "Test User1",
		DateOfBirth: "1990-09-01",
		AreaCode:    "852",
		Phone:       "85244448001",
	}
	user1UUID, _ := userRepository.CreateUser(&user1)

	user2 := models.User{
		Fullname:    "Test User2",
		DateOfBirth: "1990-09-02",
		AreaCode:    "852",
		Phone:       "85244448002",
	}
	user2UUID, _ := userRepository.CreateUser(&user2)

	user1ID, _ := userRepository.GetUserIDByUUID(user1UUID)
	user2ID, _ := userRepository.GetUserIDByUUID(user2UUID)

	connection1 := models.Connection{
		UserID1:  user1ID,
		UserID2:  user2ID,
		Relation: "friends",
	}
	connectionRepository.Create(&connection1)

	connection2 := models.Connection{
		UserID1:  user2ID,
		UserID2:  user1ID,
		Relation: "friends",
	}
	connectionRepository.Create(&connection2)

	return user1UUID, user2UUID
}

func tearDown() {
	userRepository := repositories.UserRepository{}
	connectionRepository := repositories.ConnectionRepository{}

	user1ID, _ := userRepository.GetUserIDByPhoneNumber("85244448001")
	user2ID, _ := userRepository.GetUserIDByPhoneNumber("85244448002")

	connection1 := models.Connection{
		UserID1:  user1ID,
		UserID2:  user2ID,
		Relation: "friends",
	}
	connectionRepository.Delete(&connection1)

	connection2 := models.Connection{
		UserID1:  user2ID,
		UserID2:  user1ID,
		Relation: "friends",
	}
	connectionRepository.Delete(&connection2)

	userRepository.DeleteByPhoneNumber("85244448001")
	userRepository.DeleteByPhoneNumber("85244448002")

}
