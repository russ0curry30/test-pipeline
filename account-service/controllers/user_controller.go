package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/common"
	appContext "gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/helpers"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/services"
)

// UserController the endpoints for users
type UserController struct {
}

// RegisterUserApis register user apis to router
func RegisterUserApis(router *gin.RouterGroup) {
	api := UserController{}
	router.POST("/register", api.Register)
	router.POST("/login", api.Login)
	router.GET("/profile/:uuid", api.GetProfile)
	router.POST("/profile", api.UpdateProfile)
}

// Register register a user
// @Summary Register a new user
// @Tags 2-Users
// @Produce json
// @Param body body dto.UserRegistrationRequest true "List of parameters"
// @Success 200 {string} json "{"uuid":"fake-token", "uuid":"uuid"}"
// @Router /register [post]
func (userController *UserController) Register(context *gin.Context) {
	request := dto.UserRegistrationRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[UserController.Register] Error binding json in request body: " + err.Error())
		return
	}

	uuid, err := userController.registerUser(request)
	if err != nil {
		log.Print("[UserController.Register] Error registering user: " + err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	privateKey := appContext.Configuration.Get("PRIVATE_KEY")
	validatyInHours := common.JWTValidityLengthInHours
	token, err := helpers.CreateJwt("SUBJECT", "ISSUER", privateKey, uuid, validatyInHours)
	context.JSON(http.StatusOK, gin.H{
		"uuid": uuid,
		"jwt":  token,
	})
}

// Login user login using otp and token
// @Summary Login existing user
// @Tags 2-Users
// @Description Login user using media, entity and phaseTwoToken
// @Accept  json
// @Produce json
// @Param body body dto.UserLoginRequest true "List of parameters"
// @Success 200 {string} json "{"uuid":"fake-token"}"
// @Router /login [post]
func (userController *UserController) Login(context *gin.Context) {
	request := dto.UserLoginRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[UserController.Login] Error binding json in request body: " + err.Error())
		return
	}

	uuid, err := userController.loginUser(request)
	if err != nil {
		log.Print("[UserController.Login] Error login user: " + err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	privateKey := appContext.Configuration.Get("PRIVATE_KEY")
	validatyInHours := common.JWTValidityLengthInHours
	token, err := helpers.CreateJwt("SUBJECT", "ISSUER", privateKey, uuid, validatyInHours)
	context.JSON(http.StatusOK, gin.H{
		"uuid": uuid,
		"jwt":  token,
	})
}

// GetProfile return user's profile
// @Summary User profile
// @Tags 2-Users
// @Description get User profile
// @Produce json
// @Param uuid path string true "uuid"
// @Success 200 {object} dto.UserProfileResponse
// @Router /profile/{uuid} [get]
func (userController *UserController) GetProfile(context *gin.Context) {
	uuid := context.Param("uuid")

	user, error := services.GetProfileById(uuid)
	if error != nil {
		log.Printf("[UserController.Profile] Error fetching user profile: %s for %s:", uuid, error.Error())
	}

	context.JSON(http.StatusOK, user)
}

// UpdateProfile Update user's profile
// @Summary User profile
// @Security securitydefinitions.oauth2.application
// @Tags 2-Users
// @Produce json
// @Param body body dto.UpdateProfileRequest true "List of parameters"
// @Success 200 {string} json "{"success":true}"
// @Router /profile [post]
func (userController *UserController) UpdateProfile(context *gin.Context) {
	request := dto.UpdateProfileRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[UserController.UpdateProfile] Error decoding json in request body: " + err.Error())
		return
	}

	uuid := request.UUID
	// phone := request.Phone
	// email := request.Email
	fullname := request.Fullname
	dateOfBirth := request.DateOfBirth
	nickname := request.Nickname
	status := request.Status
	avatarURL := request.AvatarURL

	err = services.UpdateProfileByUuid(uuid, fullname, dateOfBirth, nickname, status, avatarURL)

	if err != nil {
		log.Printf("[UserController.UpdateProfile] Error updating user profile: %s for %s:", uuid, err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{"success": true})
}

func (userController *UserController) registerUser(request dto.UserRegistrationRequest) (string, error) {
	phaseTwoToken := request.PhaseTwoToken
	entity := request.PhoneNumber.FormattedValue()
	areaCode := request.PhoneNumber.FormattedAreaCode()
	fullname := request.Fullname
	dob := request.DateOfBirth

	err := services.FindValidTokenThenInvalidate(phaseTwoToken, "", common.Registration, common.MediaPhone, entity)
	if err != nil {
		return "", err
	}

	userModel := userController.newUser(common.MediaPhone, entity, areaCode, fullname, dob, []byte(common.DefaultSettings))
	uuid, err := services.SaveUser(&userModel)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func (userController *UserController) loginUser(request dto.UserLoginRequest) (string, error) {
	phaseTwoToken := request.PhaseTwoToken
	entity := request.PhoneNumber.FormattedValue()

	err := services.FindValidTokenThenInvalidate(phaseTwoToken, "", "L", common.MediaPhone, entity)
	if err != nil {
		return "", err
	}

	var uuid string
	uuid, err = services.FindUserUuidByPhone(entity)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func (userController *UserController) newUser(media, entity, areaCode, fullname, dob string, settings json.RawMessage) models.User {
	user := models.User{
		Fullname:    fullname,
		DateOfBirth: dob,
		AreaCode:    areaCode,
		Phone:       entity,
		Settings:    settings,
	}

	return user
}
