package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvlidUserRequest(t *testing.T) {
	router := setupRoute()

	recorder := httptest.NewRecorder()
	jsonStr := []byte(`invalid request`)
	req, _ := http.NewRequest("POST", "/account/v1/login", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)

	assert.Equal(t, 400, recorder.Code)

	req, _ = http.NewRequest("POST", "/account/v1/register", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)
	assert.Equal(t, 400, recorder.Code)

	req, _ = http.NewRequest("GET", "/account/v1/profile/abcdefg", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorder, req)
	assert.Equal(t, 400, recorder.Code)
}

func TestLogin(t *testing.T) {
	router := setupRoute()
	_, _ = setUpData()

	// Test requestOtp endpoint
	recorderForRequestOtp := httptest.NewRecorder()
	jsonStr := []byte(`{"areaCode": "852","phone": "44448001","purpose": "L"}`)
	req, _ := http.NewRequest("POST", "/account/v1/requestOtp", bytes.NewBuffer(jsonStr))
	router.ServeHTTP(recorderForRequestOtp, req)

	assert.Equal(t, 200, recorderForRequestOtp.Code)
	assert.Contains(t, recorderForRequestOtp.Body.String(), "phaseOneToken")

	// Get the phaseOneToken from last endpoint
	token := make(map[string]string)
	json.Unmarshal(recorderForRequestOtp.Body.Bytes(), &token)

	// Test verifyOtp endpoint
	verifyRequestStr := fmt.Sprintf(
		`{
			"areaCode": "%s",
			"code": "%s",
			"phaseOneToken": "%s",
			"phone": "%s",
			"purpose": "L"
		}`,
		"852",
		"333333",
		token["phaseOneToken"],
		"44448001",
	)
	jsonStr = []byte(verifyRequestStr)
	req, _ = http.NewRequest("POST", "/account/v1/verifyOtp", bytes.NewBuffer(jsonStr))
	recorderForVerifyOtp := httptest.NewRecorder()
	router.ServeHTTP(recorderForVerifyOtp, req)

	assert.Equal(t, 200, recorderForVerifyOtp.Code)
	assert.Contains(t, recorderForVerifyOtp.Body.String(), "phaseTwoToken")

	// Get the phaseTwoToken from last endpoint
	json.Unmarshal(recorderForVerifyOtp.Body.Bytes(), &token)

	// Testing Login endpoint
	loginRequestStr := fmt.Sprintf(
		`{
			"areaCode": "%s",
  		"phaseTwoToken": "%s",
  		"phone": "%s"
		}`,
		"852",
		token["phaseTwoToken"],
		"44448001",
	)
	jsonStr = []byte(loginRequestStr)
	req, _ = http.NewRequest("POST", "/account/v1/login", bytes.NewBuffer(jsonStr))
	recorderForLogin := httptest.NewRecorder()
	router.ServeHTTP(recorderForLogin, req)

	assert.Equal(t, 200, recorderForLogin.Code)
	assert.Contains(t, recorderForLogin.Body.String(), "uuid")
	assert.Contains(t, recorderForLogin.Body.String(), "jwt")

	tearDown()
}

func TestProfile(t *testing.T) {
	router := setupRoute()
	user1UUID, _ := setUpData()

	// Test profile endpoint
	recorderForProfile := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/account/v1/profile/"+user1UUID, nil)
	router.ServeHTTP(recorderForProfile, req)

	assert.Equal(t, 200, recorderForProfile.Code)

	assert.Contains(t, recorderForProfile.Body.String(), `"uuid"`)
	assert.Contains(t, recorderForProfile.Body.String(), `"fullname":"Test User1"`)
	assert.Contains(t, recorderForProfile.Body.String(), `"dateOfBirth":"1990-09-01T00:00:00Z"`)
	assert.Contains(t, recorderForProfile.Body.String(), `"locale":""`)
	assert.Contains(t, recorderForProfile.Body.String(), `"avatarUrl":""`)

	tearDown()
}
