package crons

import (
	"errors"
	"log"
	"strconv"

	"github.com/jasonlvhit/gocron"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/external-apis/gmail"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/external-apis/twilio"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/services"
)

func Init() {
	gocron.Every(5).Second().Do(sendOtpTask)
	<-gocron.Start()
}

func sendOtpTask() {
	//Find OTPs in queue
	otps, err := services.FindQueuedOtps()
	if err != nil {
		log.Println("[OTP]: " + err.Error())
	}

	if len(otps) < 0 {
		return
	}
	for _, otp := range otps {
		if otp.Retry > 0 {
			log.Println("[OTP CRON]: Retry sending otp for " + otp.Entity + "  Number: " + strconv.Itoa(otp.Retry))
		}

		err = sendOTP(otp.Media, otp.Entity, otp.Code)
		if err != nil {
			err = services.IncrementQueueOtpRetry(otp.ID)
			if err != nil {
				log.Println("[OTP CRON]: " + err.Error())
			}
			return
		}
		err := services.UpdateQueueOtpSent(otp.ID)
		if err != nil {
			log.Println("[OTP CRON]: " + err.Error())
		}
	}
}

func sendOTP(media, entity, code string) error {
	if media == "email" {
		err := gmail.SendVerificationEmail(entity, code)
		if err != nil {
			log.Println("[Gmail]: " + err.Error())
		}
		return err

	} else if media == "phone" {
		err := twilio.SendSMS("+"+entity, code)
		if err != nil {
			log.Println("[Twilio]: " + err.Error())
		}
		return err
	}

	return errors.New("[OTP CRON]: Unknow Media")
}
