
-- +migrate Up
CREATE TABLE IF NOT EXISTS users (
    id          INT                AUTO_INCREMENT PRIMARY KEY,
    uuid        VARCHAR(36)        NOT NULL,
    area_code   VARCHAR(9)         NULL,
    phone       VARCHAR(50)        NULL,
    email       VARCHAR(255)       NULL,
    full_name   VARCHAR(255)       NOT NULL,
    dob         DATE               NOT NULL,
    nickname    VARCHAR(255)       NULL,
    locale      VARCHAR(5)         NOT NULL DEFAULT 'en-US',
    created_at  DATETIME           DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME           DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at  DATETIME           NULL,
    UNIQUE (uuid),
    INDEX `index_phone_number` (`area_code`, `phone`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tokens (
    id           INT                AUTO_INCREMENT PRIMARY KEY,
    token        VARCHAR(36)        NOT NULL DEFAULT '',
    code         VARCHAR(6)         NOT NULL DEFAULT '',
    purpose      VARCHAR(1)         NOT NULL COMMENT "V - verification, R - Registration, L - Login",
    media        VARCHAR(5)         NOT NULL COMMENT "email or phone",
    entity       VARCHAR(255)       NOT NULL,
    valid        tinyint            NOT NULL DEFAULT 1 COMMENT "1 - valid, 0 - invalid",
    created_at   DATETIME           DEFAULT CURRENT_TIMESTAMP,
    expire_at    DATETIME           NOT NULL,
    INDEX `index_token` (`token`, `code`, `purpose`, `media`, `entity`, `valid`, `expire_at`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- +migrate Down
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS tokens;
