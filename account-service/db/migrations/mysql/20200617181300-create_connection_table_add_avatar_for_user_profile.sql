
-- +migrate Up

CREATE TABLE IF NOT EXISTS connections (
    id          INT                AUTO_INCREMENT PRIMARY KEY,
    user_id1    INT                NOT NULL,
    user_id2    INT                NOT NULL,
    relation    VARCHAR(50)        NOT NULL COMMENT "Family, Friends or some other terms defined by user",
    created_at  DATETIME           DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME           DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at  DATETIME           NULL,
    INDEX `index_user_id1` (`user_id1`),
    INDEX `index_relation` (`user_id1`, `relation`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS avatars (
    id          INT                AUTO_INCREMENT PRIMARY KEY,
    user_id     INT                NOT NULL,
    url         VARCHAR(255)       NOT NULL,
    created_at  DATETIME           DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (user_id),
    INDEX `index_avatar_user_id` (`user_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

ALTER TABLE users ADD avatar_id INT NOT NULL DEFAULT 0 AFTER locale;


-- +migrate Down
ALTER TABLE users DROP COLUMN avatar_id;
DROP TABLE IF EXISTS avatars;
DROP TABLE IF EXISTS connections;
