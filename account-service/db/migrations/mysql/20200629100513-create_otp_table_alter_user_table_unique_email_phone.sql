
-- +migrate Up

CREATE TABLE IF NOT EXISTS otp_queues (
    id          INT                AUTO_INCREMENT PRIMARY KEY,
    entity      VARCHAR(255)       NOT NULL,
    media       VARCHAR(5)         NOT NULL COMMENT "email or phone",
    code        VARCHAR(6)         NOT NULL DEFAULT '',
    retry       INT                DEFAULT 0,
    status      VARCHAR(6)         NOT NULL DEFAULT "queued" COMMENT "queued, sent or failed",
    created_at  DATETIME           DEFAULT CURRENT_TIMESTAMP,
    INDEX `index_status` (`status`, `retry`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

ALTER TABLE users ADD CONSTRAINT users_unique_phone_number UNIQUE (area_code, phone);
ALTER TABLE users ADD CONSTRAINT users_unique_email UNIQUE (email);

-- +migrate Down
ALTER TABLE users DROP INDEX users_unique_email;
ALTER TABLE users DROP INDEX users_unique_phone_number;
DROP TABLE IF EXISTS otp_queues;
