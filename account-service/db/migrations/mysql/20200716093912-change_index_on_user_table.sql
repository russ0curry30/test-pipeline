-- +migrate Up
ALTER TABLE users DROP INDEX users_unique_phone_number;
ALTER TABLE users DROP INDEX index_phone_number;
ALTER TABLE users ADD CONSTRAINT users_unique_phone_number UNIQUE INDEX(phone);

-- +migrate Down
ALTER TABLE users DROP INDEX users_unique_phone_number;
ALTER TABLE users ADD CONSTRAINT users_unique_phone_number UNIQUE (area_code, phone);
