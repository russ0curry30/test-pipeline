-- +migrate Up
DELETE FROM connections WHERE user_id1 = user_id2;
SELECT @maxId := MAX(connections.id) FROM connections;
INSERT INTO connections (user_id1,user_id2, relation)
SELECT DISTINCT user_id1,user_id2, 'friend' FROM connections;
DELETE FROM connections WHERE id <= @maxId;
ALTER TABLE connections ADD UNIQUE KEY unique_key_user_id1_user_id2(user_id1, user_id2);


-- +migrate Down
ALTER TABLE connections DROP INDEX `unique_key_user_id1_user_id2`;
