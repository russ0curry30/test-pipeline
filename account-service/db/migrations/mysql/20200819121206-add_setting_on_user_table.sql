-- +migrate Up
ALTER TABLE users ADD settings JSON NOT NULL AFTER avatar_id;
UPDATE users SET settings = '{"group_msg": true, "last_seen": true, "bday_notif": true, "friend_req": true, "night_mode": false, "location_svc": "Allow while using", "in_app_sounds": true, "profile_photo": true, "read_receipts": true, "show_location": true, "in_app_preview": true, "in_app_vibrants": true, "pause_all_notif": false}';
ALTER TABLE users ADD user_status varchar(255) DEFAULT NULL AFTER settings;

-- +migrate Down
ALTER TABLE users DROP COLUMN settings;
ALTER TABLE users DROP COLUMN user_status;
