package dto

import (
	"fmt"
	"strings"
)

// PhoneNumber phone number and area code
type PhoneNumber struct {
	Phone    string `json:"phone" binding:"required"`
	AreaCode string `json:"areaCode" binding:"required"`
}

// FormattedValue get formatted phone number with area code
func (phoneNumber *PhoneNumber) FormattedValue() string {
	return fmt.Sprintf("%s%s", phoneNumber.FormattedAreaCode(), phoneNumber.formattedPhone())
}

// FormattedAreaCode get formated area code
func (phoneNumber *PhoneNumber) FormattedAreaCode() string {
	return strings.ReplaceAll(phoneNumber.AreaCode, "+", "")
}

// formattedPhone get formated phone without area code
func (phoneNumber *PhoneNumber) formattedPhone() string {
	return strings.ReplaceAll(strings.ReplaceAll(phoneNumber.Phone, "-", ""), " ", "")
}

// Email email
type Email struct {
	Email string `json:"email" binding:"required"`
}
