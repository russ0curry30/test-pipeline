package dto

// OtpRequest request dto for otp
type OtpRequest struct {
	PhoneNumber
	Purpose string `json:"purpose" binding:"required,oneof=R L"`
}

// VerifyOtpRequest request dto for verifyOtp
type VerifyOtpRequest struct {
	PhoneNumber
	Purpose       string `json:"purpose" binding:"required,oneof=R L"`
	Code          string `json:"code" binding:"required"`
	PhaseOneToken string `json:"phaseOneToken" binding:"required"`
}

// UserRegistrationRequest request dto for user registeration
type UserRegistrationRequest struct {
	PhoneNumber
	Fullname      string `json:"fullname" binding:"required"`
	DateOfBirth   string `json:"dateOfBirth" binding:"required" time_format:"2006-01-02"`
	PhaseTwoToken string `json:"phaseTwoToken" binding:"required"`
}

// UserLoginRequest request dto for user login
type UserLoginRequest struct {
	PhoneNumber
	PhaseTwoToken string `json:"phaseTwoToken" binding:"required"`
}

// ConnectionRequest request dto for adding friends
type ConnectionRequest struct {
	UUID1    string `json:"uuid1" binding:"required"`
	UUID2    string `json:"uuid2" binding:"required"`
	Relation string `json:"relation" binding:"required"`
}

// ContactSearchRequest request dto for contact search
type ContactSearchRequest struct {
	UUID         string   `json:"uuid" binding:"required"`
	PhoneNumbers []string `json:"contacts" binding:"required"`
}

// Settings settings type
type Settings struct {
	PauseAllNotif *bool   `json:"pause_all_notif,omitempty"`
	GroupMsg      *bool   `json:"group_msg,omitempty"`
	FriendReq     *bool   `json:"friend_req,omitempty"`
	BdayNotif     *bool   `json:"bday_notif,omitempty"`
	Preview       *bool   `json:"in_app_preview,omitempty"`
	Sounds        *bool   `json:"in_app_sounds,omitempty"`
	Vibrants      *bool   `json:"in_app_vibrants,omitempty"`
	LastSeen      *bool   `json:"last_seen,omitempty"`
	Photo         *bool   `json:"profile_photo,omitempty"`
	Location      *bool   `json:"show_location,omitempty"`
	Receipt       *bool   `json:"read_receipts,omitempty"`
	LocationSvc   *string `json:"location_svc,omitempty"`
	NightMode     *bool   `json:"night_mode,omitempty"`
}

// GetSettingsRequest request dto for geting settings
type GetSettingsRequest struct {
	UUID string `json:"uuid" binding:"required"`
}

// UpdateSettingsRequest request dto for updating settings
type UpdateSettingsRequest struct {
	UUID     string   `json:"uuid" binding:"required"`
	Settings Settings `json:"settings" binding:"required"`
}

// UpdateProfileRequest request dto for updating user profile
type UpdateProfileRequest struct {
	UUID        string `json:"uuid" binding:"required"`
	Fullname    string `json:"fullname"`
	Status      string `json:"status"`
	DateOfBirth string `json:"dateOfBirth" time_format:"2006-01-02"`
	Nickname    string `json:"nickname"`
	AvatarURL   string `json:"avatarUrl"`
}
