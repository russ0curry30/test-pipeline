package dto

// HTTPResponse http response common
type HTTPResponse struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// OtpTokenResponse response for phase one and phase two token
type OtpTokenResponse struct {
	Token string `json:"token"`
}

// UserProfileResponse response dto for user profile
type UserProfileResponse struct {
	UUID        string `json:"uuid"`
	Fullname    string `json:"fullname"`
	DateOfBirth string `json:"dateOfBirth" time_format:"2006-01-02"`
	Nickname    string `json:"nickname"`
	Locale      string `json:"locale"`
	AvatarURL   string `json:"avatarUrl"`
	Phone       string `json:"phone"`
	Email       string `json:"email"`
	Status      string `json:"status"`
}

// LoginResponse response for login and register
type LoginResponse struct {
	UserProfileResponse
	JWT string `json:"jwt"`
}

// ContactSearchResponse response dto for contact search
type ContactSearchResponse struct {
	Connected    []UserProfileResponse
	UnConnected  []UserProfileResponse
	UnRegistered []string
}

// ConnectionResponse response dto for connections
type ConnectionResponse struct {
	Connections []UserProfileResponse
}

// SettingsResponse response dto for settings
type SettingsResponse struct {
	Settings Settings `json:"settings"`
}
