package gmail

import (
	"bytes"
	"fmt"
	"mime/quotedprintable"
	"net/smtp"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
)

// smtpServer data to smtp server
type smtpServer struct {
	host string
	port string
}

// Address URI to smtp server
func (s *smtpServer) Address() string {
	return s.host + ":" + s.port
}

func SendVerificationEmail(email, code string) error {
	// Sender data.
	from_email := context.Configuration.Get("GMAIL_USERNAME")
	password := context.Configuration.Get("GMAIL_PASSWORD")

	header := make(map[string]string)
	header["From"] = from_email
	header["Subject"] = "Your CircleYY One time password"
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", "text/html")
	header["Content-Disposition"] = "inline"
	header["Content-Transfer-Encoding"] = "quoted-printable"

	header_message := ""
	for key, value := range header {
		header_message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	body := "<h3>" + code + "</h3>"
	var body_message bytes.Buffer
	temp := quotedprintable.NewWriter(&body_message)
	temp.Write([]byte(body))
	temp.Close()

	final_message := header_message + "\r\n" + body_message.String()

	// Receiver email address.
	to := []string{
		email,
	}
	// smtp server configuration.
	smtpServer := smtpServer{host: "smtp.gmail.com", port: "587"}
	// Message.
	message := []byte(final_message)
	// Authentication.
	auth := smtp.PlainAuth("", from_email, password, smtpServer.host)
	// Sending email.
	err := smtp.SendMail(smtpServer.Address(), auth, from_email, to, message)

	return err
}
