package twilio

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
)

type TwilioError struct {
	Code *int   `json:"code"`
	Msg  string `json:"message"`
}

func SendSMS(phoneNum, code string) error {
	accountSid := context.Configuration.Get("TWILIO_ACCOUNT_SID")
	authToken := context.Configuration.Get("TWILIO_AUTH_TOKEN")
	twilioNum := context.Configuration.Get("TWILIO_PHONE_NUM")
	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json"

	v := url.Values{}
	v.Set("To", phoneNum)
	v.Set("From", twilioNum)
	v.Set("Body", "One time password for CircleYY: "+code)
	rb := *strings.NewReader(v.Encode())

	client := &http.Client{}

	req, err := http.NewRequest("POST", urlStr, &rb)
	if err != nil {
		log.Println("[Twilio Error] " + err.Error())
	}

	req.SetBasicAuth(accountSid, authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		var twilioError TwilioError
		json.NewDecoder(resp.Body).Decode(&twilioError)

		return errors.New(twilioError.Msg)
	}

	return nil
}
