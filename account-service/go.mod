module gitlab.com/circleyy_dev/backend/circleyy-services/account-service

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3-0.20200525134706-5e40c1d49c21
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.2.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/imylam/modules-go v0.0.0-20200609035102-c9c80db1c11c
	github.com/jasonlvhit/gocron v0.0.0-20200423141508-ab84337f7963
	github.com/jinzhu/gorm v1.9.14
	github.com/rubenv/sql-migrate v0.0.0-20200429072036-ae26b214fa43
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.4.0
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.3
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
)
