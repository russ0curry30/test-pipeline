package helpers

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// CreateJwt generate jwt
func CreateJwt(subject, issuer, privateKey, uuid string, tokenValidHours int) (string, error) {
	expiresTime := time.Now().Unix() + int64(tokenValidHours)*3600
	claims := jwt.StandardClaims{
		Audience:  uuid,
		ExpiresAt: expiresTime,
		Id:        uuid,
		IssuedAt:  time.Now().Unix(),
		Issuer:    issuer,
		NotBefore: time.Now().Unix(),
		Subject:   subject,
	}

	var signBytes = []byte(privateKey)
	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return "", err
	}

	rawToken := jwt.New(jwt.SigningMethodRS256)
	rawToken.Claims = claims
	token, err := rawToken.SignedString(signKey)

	return token, err
}
