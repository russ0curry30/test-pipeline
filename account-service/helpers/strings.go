package helpers

import (
	"math/rand"
	"regexp"
	"time"
)

const (
	charset  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	digitSet = "0123456789"

	regexEmail       = `^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)`
	regexPhoneNumber = `\d{8,10}`
)

var (
	seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
)

// RandomDigits generates a random string with given length
func RandomDigits(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = digitSet[seededRand.Intn(len(digitSet))]
	}
	return string(b)
}

// RandomString generates a random string with given length
func RandomString(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// IsEmail checks if a give string is in email format
func IsEmail(testString string) bool {
	match, err := regexp.MatchString(regexEmail, testString)
	if err != nil {
		return false
	}

	return match
}

// IsPhoneNumber checks if a give string is 8-10 digits
func IsPhoneNumber(testString string) bool {
	match, err := regexp.MatchString(regexPhoneNumber, testString)
	if err != nil {
		return false
	}

	return match
}
