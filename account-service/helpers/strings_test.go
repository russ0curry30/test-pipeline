package helpers

import (
	"regexp"
	"testing"
)

func TestRandomDigits(t *testing.T) {
	var digitCheck = regexp.MustCompile(`^[0-9]+$`)

	var randDigitsString string
	for i := 1; i < 100; i++ {
		randDigitsString = RandomDigits(i)
		stringLength := len(randDigitsString)

		if stringLength != i {
			t.Errorf("TestRandomDigits() failed, expected string length: %d, got %d", i, stringLength)
		}

		if !digitCheck.MatchString(randDigitsString) {
			t.Errorf("TestRandomDigits() failed, string generated is not digits: %s", randDigitsString)
		}
	}
}

func TestRandomString(t *testing.T) {
	var randString string
	for i := 0; i < 100; i++ {
		randString = RandomString(i)
		stringLength := len(randString)

		if stringLength != i {
			t.Errorf("TestRandomString() failed, expected string length: %d, got %d", i, stringLength)
		}
	}
}

func TestIsEmail(t *testing.T) {
	email := "test@test.com"
	isEmail := IsEmail(email)
	if !isEmail {
		t.Errorf("TestIsEmail() failed, testing %s is expected to be true", email)
	}

	email = "test@test"
	isEmail = IsEmail(email)
	if isEmail {
		t.Errorf("TestIsEmail() failed, testing %s is expected to be false", email)
	}

	email = "testtest.co"
	isEmail = IsEmail(email)
	if isEmail {
		t.Errorf("TestIsEmail() failed, testing %s is expected to be false", email)
	}

	email = "@test.com"
	isEmail = IsEmail(email)
	if isEmail {
		t.Errorf("TestIsEmail() failed, testing %s is expected to be false", email)
	}
}
