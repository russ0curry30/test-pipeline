package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/crons"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/route"
)

// @title Account Service API
// @version 1.0
// @description This is API Documentation for Account Service

// @termsOfService http://swagger.io/terms/

// @contact.name CircleYY
// @contact.url http://www.circleyy.io
// @contact.email support@circleyy.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /account/v1
func main() {
	err := context.Configuration.Init()
	if err != nil {
		log.Fatal("Error when init configs:", err)
	}

	context.DB.Migrate()
	context.DB.InitDBConn()

	go crons.Init()

	if context.Configuration.Get("ENV") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	router := route.InitRoutes()
	log.Println("Starting the server...")
	router.Run(":8080")
}
