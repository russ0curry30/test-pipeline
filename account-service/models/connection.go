package models

// Connection new connection model
type Connection struct {
	UserID1  int64
	UserID2  int64
	Relation string
}
