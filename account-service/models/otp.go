package models

import (
	"time"
)

const (
	StatusFailed string = "failed"
	StatusQueued string = "queued"
	StatusSent   string = "sent"
)

type Otp struct {
	ID        int       `db:"id"`
	Code      string    `db:"code"`
	Media     string    `db:"media"`
	Entity    string    `db:"entity"`
	Retry     int       `db:"retry"`
	Status    string    `db:"status"`
	CreatedAt time.Time `db:"created_at"`
}
