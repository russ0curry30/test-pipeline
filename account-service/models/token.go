package models

import (
	"time"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/helpers"
)

const (
	timeStampLayout string = "2006-01-02 15:04:05"
)

// Token model
type Token struct {
	ID        int       `db:"id"`
	Token     string    `db:"token"`
	Code      string    `db:"code"`
	Purpose   string    `db:"purpose"`
	Media     string    `db:"media"`
	Entity    string    `db:"entity"`
	Valid     bool      `db:"valid"`
	CreatedAt time.Time `db:"created_at"`
	ExpireAt  time.Time `db:"expire_at"`
}

func NewToken(code, purpose, media, entity string) Token {
	currentTime := time.Now().UTC()
	expireTime := currentTime.Add(time.Minute * 10)

	token := Token{
		Token:     helpers.RandomString(36),
		Code:      code,
		Purpose:   purpose,
		Media:     media,
		Entity:    entity,
		Valid:     true,
		CreatedAt: currentTime,
		ExpireAt:  expireTime,
	}

	return token
}
