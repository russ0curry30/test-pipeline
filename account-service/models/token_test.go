package models

import (
	"testing"
)

func TestNewToken(t *testing.T) {
	media := "phone"
	entity := "12341234"
	code := "123456"
	purpose := "R"

	newToken := NewToken(code, purpose, media, entity)

	if newToken.Code != "123456" {
		t.Errorf("Gen token error(token) = %s;", newToken.Code)
	}
}
