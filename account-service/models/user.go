package models

import (
	"encoding/json"
	"time"
)

// User model
type User struct {
	ID          int64           `db:"user_id" json:"-"`
	UUID        string          `db:"uuid" json:"uuid"`
	AreaCode    string          `db:"area_code" json:"-"`
	Phone       string          `db:"phone" json:"phone"`
	Email       string          `db:"email" json:"email"`
	Fullname    string          `db:"full_name" json:"fullname"`
	Status      string          `db:"user_status" json:"status"`
	DateOfBirth string          `db:"dob" json:"dateOfBirth"`
	NickName    string          `db:"nickname" json:"nickname"`
	Locale      string          `db:"locale" json:"locale"`
	Settings    json.RawMessage `db:"settings" json:"settings"`
	AvatarID    string          `db:"avatar_id" json:"-"`
	AvatarURL   string          `db:"avatar_url" json:"avatarUrl"`
	CreatedAt   time.Time       `db:"created_at" json:"-"`
	UpdatedAt   time.Time       `db:"updated_at" json:"-"`
	DeletedAt   time.Time       `db:"deleted_at" json:"-"`
}
