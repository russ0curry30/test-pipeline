# How to start the application
- You need a mysql running.
- `cp account-service_local-example.json account-service_local.json`
- Change your configuration
- `go run main.go`
- go to api documentation page: http://127.0.0.1:8080/account/v1/swagger/index.html



# How to run testcase
```
go test -v -coverpkg ./... ./... -coverprofile coverage.out
go tool cover -html coverage.out -o cover.html
```
the coverage report will be generated in cover.html

##### Notice:
For the API testing, we are using a real database, so you need to change your configuration in account-service_unittest.json

<We know that it's weird to put database configuration into unittest config, we will change that later on>


# How to start the application(Docker way)
TO BE ADDED
