package repositories

import (
	"log"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
)

// ConnectionRepository repository for connection
type ConnectionRepository struct {
}

// Create delete user by phonenumber
func (connectionRepository *ConnectionRepository) Create(connection *models.Connection) error {
	dbConn := context.DB.GetDBConnection()
	q := "INSERT INTO connections (user_id1, user_id2, relation) VALUES (?, ?, ?)"
	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}
	_, err = insertStmt.Exec(connection.UserID1, connection.UserID2, connection.Relation)
	return err
}

// Delete delete connection
func (connectionRepository *ConnectionRepository) Delete(connection *models.Connection) error {
	dbConn := context.DB.GetDBConnection()
	q := "DELETE FROM connections WHERE user_id1 = ? AND user_id2 = ? AND relation = ?"
	deleteStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}
	_, err = deleteStmt.Exec(connection.UserID1, connection.UserID2, connection.Relation)

	return err
}

// GetConnectionsByUserID get connections by User ID
func (connectionRepository *ConnectionRepository) GetConnectionsByUserID(userID int64) []models.Connection {
	dbConn := context.DB.GetDBConnection()
	var connections []models.Connection
	querySmt, err := dbConn.Prepare("SELECT `user_id2` FROM `connections` WHERE user_id1 = ?")
	rows, err := querySmt.Query(userID)
	if err != nil {
		panic(err.Error())
	}

	for rows.Next() {
		var connection models.Connection
		if err := rows.Scan(&connection.UserID2); err != nil {
			log.Println(err.Error())
		}
		connections = append(connections, connection)
	}

	return connections
}
