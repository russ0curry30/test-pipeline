package repositories

import (
	"database/sql"
	"log"
	"strings"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/common"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
)

// UserRepository repository for user table
type UserRepository struct {
}

// DeleteByPhoneNumber delete user by phonenumber
func (userRepository *UserRepository) DeleteByPhoneNumber(phoneNumber string) error {
	dbConn := context.DB.GetDBConnection()

	q := "DELETE FROM users WHERE phone=?"
	deleteStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}
	_, err = deleteStmt.Exec(phoneNumber)
	return err
}

// CreateUser save user into database and return the uuid
func (userRepository *UserRepository) CreateUser(user *models.User) (string, error) {
	dbConn := context.DB.GetDBConnection()

	q := "INSERT INTO users (uuid, area_code, phone, email, full_name, dob, settings)" +
		"VALUES (uuid(), ?, ?, ?, ?, ?, ?)"
	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}

	setting := user.Settings
	if len(setting) == 0 {
		setting = []byte(common.DefaultSettings)
	}
	result, err := insertStmt.Exec(
		nullString(user.AreaCode), nullString(user.Phone), nullString(user.Email), user.Fullname, user.DateOfBirth, setting)
	if err != nil {
		return "", err
	}
	userID, err := result.LastInsertId()
	if err != nil {
		return "", err
	}
	uuid, err := userRepository.GetUUIDByUserID(userID)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

// GetUUIDByUserID save user into database and return the uuid
func (userRepository *UserRepository) GetUUIDByUserID(userID int64) (string, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT uuid FROM users WHERE id=?"
	stmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}

	var uuid string
	err = stmt.QueryRow(userID).Scan(&uuid)
	return uuid, err
}

// GetUserIDByUUID get userID by phoneNumber
func (userRepository *UserRepository) GetUserIDByUUID(uuid string) (int64, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT id FROM users WHERE uuid=?"
	stmt, _ := dbConn.Prepare(q)
	var userID int64
	err := stmt.QueryRow(uuid).Scan(&userID)
	return userID, err
}

// GetUserIDByPhoneNumber get userID by phoneNumber
func (userRepository *UserRepository) GetUserIDByPhoneNumber(phoneNumber string) (int64, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT id FROM users WHERE phone=?"
	stmt, _ := dbConn.Prepare(q)
	var userID int64
	err := stmt.QueryRow(phoneNumber).Scan(&userID)
	return userID, err
}

// GetExistedUsers get exsited user by phone numbers
func (userRepository *UserRepository) GetExistedUsers(phoneNumbers []string) []models.User {
	dbConn := context.DB.GetDBConnection()
	var users []models.User

	args := make([]interface{}, len(phoneNumbers))
	for i, phoneNumber := range phoneNumbers {
		args[i] = phoneNumber
	}

	q := `SELECT u.id,
	uuid,
COALESCE(area_code, '') as area_code,
COALESCE(phone, '') as phone,
COALESCE(email, '') as email,
full_name,
dob,
COALESCE(nickname, '') as nickname,
COALESCE(ava.url, '') as avatar_url
FROM users u
left join avatars ava
on ava.id = u.avatar_id
WHERE u.phone in (?` + strings.Repeat(",?", len(args)-1) + `)`
	rows, err := dbConn.Query(q, args...)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.ID, &user.UUID, &user.AreaCode, &user.Phone, &user.Email, &user.Fullname, &user.DateOfBirth, &user.NickName, &user.AvatarURL); err != nil {
			log.Println(err.Error())
		}
		users = append(users, user)
	}

	return users
}

// GetUsersByIDs get list of users by IDs
func (userRepository *UserRepository) GetUsersByIDs(IDs []int64) []models.User {
	dbConn := context.DB.GetDBConnection()
	var users []models.User

	args := make([]interface{}, len(IDs))
	for i, id := range IDs {
		args[i] = id
	}

	q := `SELECT u.id,
	uuid,
COALESCE(area_code, '') as area_code,
COALESCE(phone, '') as phone,
COALESCE(email, '') as email,
full_name,
dob,
COALESCE(nickname, '') as nickname,
COALESCE(ava.url, '') as avatar_url
FROM users u
left join avatars ava
on ava.id = u.avatar_id
WHERE u.id in (?` + strings.Repeat(",?", len(args)-1) + `)`
	rows, err := dbConn.Query(q, args...)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.ID, &user.UUID, &user.AreaCode, &user.Phone, &user.Email, &user.Fullname, &user.DateOfBirth, &user.NickName, &user.AvatarURL); err != nil {
			log.Println(err.Error())
		}
		users = append(users, user)
	}

	return users
}

func nullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
