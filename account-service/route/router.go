package route

import (
	"fmt"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/controllers"
	_ "gitlab.com/circleyy_dev/backend/circleyy-services/account-service/docs"
)

// InitRoutes setup the routers
func InitRoutes() *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))
	router.Use(gin.Recovery())

	v1 := router.Group("/account/v1")
	{
		v1.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	optAPIGroup := router.Group("/account/v1")
	controllers.RegisterOtpApis(optAPIGroup)
	controllers.RegisterUserApis(optAPIGroup)

	connectionAPIGroup := router.Group("/account/v1/connection")
	controllers.RegisterConnectionApis(connectionAPIGroup)

	settingsAPIGroup := router.Group("/account/v1/settings")
	controllers.SettingsApis(settingsAPIGroup)

	return router
}
