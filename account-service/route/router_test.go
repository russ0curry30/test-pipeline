package route

import (
	"testing"
)

func TestInitRouters(t *testing.T) {
	router := InitRoutes()

	group := router.Group("/account/v1")
	if group.BasePath() != "/account/v1" {
		t.Errorf("Test failed on router group")
	}
}
