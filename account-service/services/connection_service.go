package services

import (
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/repositories"
)

// ConnectionService ConnectionService
type ConnectionService struct {
	repositories.UserRepository
	repositories.ConnectionRepository
}

// AddConnection add connection
func (connectionService *ConnectionService) AddConnection(connectionRequest *dto.ConnectionRequest) {
	userID1, _ := connectionService.UserRepository.GetUserIDByUUID(connectionRequest.UUID1)
	userID2, _ := connectionService.UserRepository.GetUserIDByUUID(connectionRequest.UUID2)
	connection1 := models.Connection{
		UserID1:  userID1,
		UserID2:  userID2,
		Relation: connectionRequest.Relation,
	}
	connection2 := models.Connection{
		UserID1:  userID2,
		UserID2:  userID1,
		Relation: connectionRequest.Relation,
	}
	connectionService.ConnectionRepository.Create(&connection1)
	connectionService.ConnectionRepository.Create(&connection2)
}

// SearchByContacts search connectios by phone numbers
func (connectionService *ConnectionService) SearchByContacts(contactSearchRequest *dto.ContactSearchRequest) dto.ContactSearchResponse {
	userID, _ := connectionService.UserRepository.GetUserIDByUUID(contactSearchRequest.UUID)

	connections := connectionService.ConnectionRepository.GetConnectionsByUserID(userID)
	existingUsers := connectionService.UserRepository.GetExistedUsers(contactSearchRequest.PhoneNumbers)

	response := dto.ContactSearchResponse{
		Connected:    []dto.UserProfileResponse{},
		UnConnected:  []dto.UserProfileResponse{},
		UnRegistered: []string{},
	}
	for index := 0; index < len(contactSearchRequest.PhoneNumbers); index++ {
		registered, connected, userProfile := false, false, models.User{}
		for indexOfUsers := 0; indexOfUsers < len(existingUsers); indexOfUsers++ {
			if contactSearchRequest.PhoneNumbers[index] == existingUsers[indexOfUsers].Phone {
				registered = true
				userProfile = existingUsers[indexOfUsers]
				break
			}
		}

		if registered {
			for indexOfConnections := 0; indexOfConnections < len(connections); indexOfConnections++ {
				if userProfile.ID == connections[indexOfConnections].UserID2 {
					connected = true
					break
				}
			}
		}

		if connected {
			response.Connected = append(response.Connected, dto.UserProfileResponse{
				UUID:        userProfile.UUID,
				Fullname:    userProfile.Fullname,
				DateOfBirth: userProfile.DateOfBirth,
				Nickname:    userProfile.NickName,
				Locale:      userProfile.Locale,
				AvatarURL:   "",
			})
		} else if registered {
			response.UnConnected = append(response.UnConnected, dto.UserProfileResponse{
				UUID:        userProfile.UUID,
				Fullname:    userProfile.Fullname,
				DateOfBirth: userProfile.DateOfBirth,
				Nickname:    userProfile.NickName,
				Locale:      userProfile.Locale,
				AvatarURL:   "",
			})
		} else {
			response.UnRegistered = append(response.UnRegistered, contactSearchRequest.PhoneNumbers[index])
		}

	}
	return response
}

// ListConnections return a list of user profiles for user
func (connectionService *ConnectionService) ListConnections(uuid string) dto.ConnectionResponse {
	connectionResponse := dto.ConnectionResponse{
		Connections: []dto.UserProfileResponse{},
	}
	userID, _ := connectionService.UserRepository.GetUserIDByUUID(uuid)
	connections := connectionService.ConnectionRepository.GetConnectionsByUserID(userID)
	connectionUserIDs := make([]int64, 8)
	for index := 0; index < len(connections); index++ {
		connectionUserIDs = append(connectionUserIDs, connections[index].UserID2)
	}

	users := connectionService.UserRepository.GetUsersByIDs(connectionUserIDs)

	for index := 0; index < len(users); index++ {
		connectionResponse.Connections = append(connectionResponse.Connections, dto.UserProfileResponse{
			UUID:        users[index].UUID,
			Fullname:    users[index].Fullname,
			DateOfBirth: users[index].DateOfBirth,
			Nickname:    users[index].NickName,
			Locale:      users[index].Locale,
			AvatarURL:   "",
		})
	}
	return connectionResponse
}
