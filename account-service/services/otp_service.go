package services

import (
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
)

func AddOtpQueue(entity, media, code string) error {
	q := "INSERT INTO otp_queues (entity, media, code) VALUES(?,?,?)"

	dbConn := context.DB.GetDBConnection()

	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}

	_, err = insertStmt.Exec(entity, media, code)
	if err != nil {
		return err
	}

	return nil
}

func FindQueuedOtps() ([]models.Otp, error) {
	dbConn := context.DB.GetDBConnection()

	rows, err := dbConn.Query("Select * FROM otp_queues WHERE status='queued' AND retry <=3")
	if err != nil {
		return nil, err
	}

	var otps []models.Otp
	for rows.Next() {
		var otp models.Otp
		err = rows.Scan(
			&otp.ID, &otp.Entity, &otp.Media, &otp.Code, &otp.Retry, &otp.Status, &otp.CreatedAt)
		if err != nil {
			return nil, err
		}
		otps = append(otps, otp)
	}

	return otps, nil
}

func UpdateQueueOtpSent(id int) error {
	q := "UPDATE otp_queues SET status = 'sent' WHERE id=?"

	dbConn := context.DB.GetDBConnection()

	updateStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}

	_, err = updateStmt.Exec(id)
	if err != nil {
		return err
	}

	return nil
}

func IncrementQueueOtpRetry(id int) error {
	q := "UPDATE otp_queues SET retry = retry + 1 WHERE id=?"

	dbConn := context.DB.GetDBConnection()

	updateStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}

	_, err = updateStmt.Exec(id)
	if err != nil {
		return err
	}

	return nil
}
