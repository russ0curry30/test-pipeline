package services

import (
	"log"
	"time"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
)

func SaveToken(token *models.Token) (*models.Token, error) {
	q := "INSERT INTO tokens (token, code, purpose, media, entity, valid, created_at, expire_at)" +
		"VALUES(?,?,?,?,?,?,?,?)"

	dbConn := context.DB.GetDBConnection()

	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return nil, err
	}
	//TODO: Get and return the inserted token
	log.Println(token.ExpireAt)
	_, err = insertStmt.Exec(token.Token, token.Code, token.Purpose,
		token.Media, token.Entity, token.Valid, token.CreatedAt, token.ExpireAt)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func FindValidTokenThenInvalidate(token, code, purpose, media, entity string) error {
	validToken, err := findValidToken(token, code, purpose, media, entity)
	if err != nil {
		return err
	}

	err = invalidateTokenById(validToken.ID)
	if err != nil {
		return err
	}

	return nil
}

func findValidToken(token, code, purpose, media, entity string) (*models.Token, error) {
	q := "Select id, media, entity FROM tokens " +
		"WHERE token=? AND code=? AND purpose=? " +
		"AND media=? AND entity=? AND valid=true AND expire_at>=?"

	dbConn := context.DB.GetDBConnection()

	findByTokenAndCodeStmt, err := dbConn.Prepare(q)
	if err != nil {
		return nil, err
	}

	var verToken models.Token
	err = findByTokenAndCodeStmt.QueryRow(token, code, purpose,
		media, entity, time.Now().UTC()).Scan(&verToken.ID, &verToken.Media, &verToken.Entity)
	if err != nil {
		return nil, err
	}

	return &verToken, nil
}

func invalidateTokenById(id int) error {
	q := "UPDATE tokens SET valid=false WHERE id=?"

	dbConn := context.DB.GetDBConnection()

	setTokenValidStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}

	_, err = setTokenValidStmt.Exec(id)
	if err != nil {
		return err
	}

	return nil
}
