package services

import (
	"database/sql"

	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/account-service/models"
)

func SaveUser(user *models.User) (string, error) {
	dbConn := context.DB.GetDBConnection()

	q := "INSERT INTO users (uuid, area_code, phone, email, full_name, dob, settings)" +
		"VALUES (uuid(), ?, ?, ?, ?, ?, ?)"
	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}
	result, err := insertStmt.Exec(
		nullString(user.AreaCode), nullString(user.Phone), nullString(user.Email), user.Fullname, user.DateOfBirth, user.Settings)
	if err != nil {
		return "", err
	}
	userID, err := result.LastInsertId()
	if err != nil {
		return "", err
	}
	uuid, err := FindUserUuidById(userID)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func FindUserUuidByPhone(phoneNum string) (string, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT uuid FROM users WHERE phone=?"
	findUserByPhoneStmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}

	var uuid string
	// err = findUserByIDStmt.QueryRow(phoneNum).Scan(
	//     &user.ID, &user.Uuid, &user.AreaCode, &user.Phone, &user.Email, &user.Fullname, &user.DateOfBirth,
	//     &user.NickName, &user.Locale, &user.AvatarID, &user.CreatedAt, &user.UpdatedAt, &user.DeletedAt)
	err = findUserByPhoneStmt.QueryRow(phoneNum).Scan(&uuid)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func FindUserUuidByEmail(email string) (string, error) {
	dbConn := context.DB.GetDBConnection()

	// q := "SELECT * FROM users WHERE area_code='852' AND phone=?"
	q := "SELECT uuid FROM users WHERE email=?"
	findUserByEmailStmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}

	var uuid string
	// err = findUserByIDStmt.QueryRow(phoneNum).Scan(
	//     &user.ID, &user.Uuid, &user.AreaCode, &user.Phone, &user.Email, &user.Fullname, &user.DateOfBirth,
	//     &user.NickName, &user.Locale, &user.AvatarID, &user.CreatedAt, &user.UpdatedAt, &user.DeletedAt)
	err = findUserByEmailStmt.QueryRow(email).Scan(&uuid)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func FindUserUuidById(id int64) (string, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT uuid FROM users WHERE id=?"
	findUserByIDStmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}

	var uuid string
	err = findUserByIDStmt.QueryRow(id).Scan(&uuid)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func FindAvatarIdByUerId(userId int) (int, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT id FROM avatars WHERE user_id=?"
	findAvatarIdByUserIdStmt, err := dbConn.Prepare(q)
	if err != nil {
		return 0, err
	}

	var id int
	err = findAvatarIdByUserIdStmt.QueryRow(userId).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func FindUserIdByUuid(uuid string) (int, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT id FROM users WHERE uuid=?"
	findUserByIDStmt, err := dbConn.Prepare(q)
	if err != nil {
		return 0, err
	}

	var id int
	err = findUserByIDStmt.QueryRow(uuid).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func GetProfileById(uuid string) (models.User, error) {
	user := models.User{}
	dbConn := context.DB.GetDBConnection()
	q := `SELECT uuid,
COALESCE(area_code, '') as area_code,
COALESCE(phone, '') as phone,
COALESCE(email, '') as email,
COALESCE(user_status, '') as status,
full_name,
dob,
COALESCE(nickname, '') as nickname,
COALESCE(ava.url, '') as avatar_url
FROM users u
left join avatars ava
on ava.id = u.avatar_id
WHERE uuid=?`
	findUserByUUID, err := dbConn.Prepare(q)
	if err != nil {
		return user, err
	}
	err = findUserByUUID.QueryRow(uuid).Scan(&user.UUID, &user.AreaCode, &user.Phone, &user.Email, &user.Status, &user.Fullname, &user.DateOfBirth, &user.NickName, &user.AvatarURL)
	return user, err
}

func UpdateProfileByUuid(uuid, fullname, dateOfBirth, nickname, status, avatarUrl string) error {
	dbConn := context.DB.GetDBConnection()

	userID, err := FindUserIdByUuid(uuid)
	if err != nil {
		return err
	}

	q := `INSERT INTO avatars( user_id, url) VALUES(?, ?) ON DUPLICATE KEY UPDATE url=?`
	updateAvatarByUUIDStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}
	_, err = updateAvatarByUUIDStmt.Exec(userID, avatarUrl, avatarUrl)
	if err != nil {
		return err
	}

	avatarId, err := FindAvatarIdByUerId(userID)
	if err != nil {
		return err
	}

	q = `UPDATE users SET full_name=?, dob=?, nickname=?, user_status=?, avatar_id=?  WHERE id = ?`
	updateProfileByUUIDStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}
	_, err = updateProfileByUUIDStmt.Exec(fullname, dateOfBirth, nickname, status, avatarId, userID)
	if err != nil {
		return err
	}

	return nil
}

func GetSettingsByUuid(uuid string) ([]byte, error) {
	dbConn := context.DB.GetDBConnection()

	q := "SELECT settings FROM users WHERE uuid=?"
	getSettingsByUUIDStmt, err := dbConn.Prepare(q)
	if err != nil {
		return []byte("{}"), err
	}

	var settings []byte
	err = getSettingsByUUIDStmt.QueryRow(uuid).Scan(&settings)
	if err != nil {
		return []byte("{}"), err
	}

	return settings, nil
}

func UpdateSettingByUuid(uuid string, newSettings []byte) error {
	dbConn := context.DB.GetDBConnection()

	q := "UPDATE users SET settings = JSON_MERGE_PATCH(settings, ?) WHERE uuid=?"
	updateStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}

	_, err = updateStmt.Exec(newSettings, uuid)
	if err != nil {
		return err
	}

	return nil
}

func nullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
