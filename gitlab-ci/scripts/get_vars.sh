#!/bin/bash

set -e -o pipefail

IFS='&' read -ra envs <<< $1
for env in "${envs[@]}"
do
    DATA="$(vault kv get -format=json kv/${SERVICE}_${env}| jq -r .data.data)"
    echo $DATA > /opt/gitlab-runner/${SERVICE}_${env}.json
    echo "Got ${SERVICE}_${env}.json"
done
