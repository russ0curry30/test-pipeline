#!/usr/bin/env bash

set -e -o pipefail
cd $CI_PROJECT_DIR/k8s/overlays/$1

for file in *.yml ; do
    SERVICE=$(echo "${file%%.*}")
    if grep -q THIS_STRING_IS_REPLACED_DURING_BUILD "$file"; then
       LATEST_TAG=$(aws ecr describe-images --repository-name $SERVICE --query 'sort_by(imageDetails,& imagePushedAt)[-1].imageTags[0]' |  sed "s/\"//g") 
       sed -ie "s/THIS_STRING_IS_REPLACED_DURING_BUILD/$AWS_ECR_REGISTRY\/$SERVICE:$LATEST_TAG/g" "$CI_PROJECT_DIR/k8s/overlays/$1/$file"
    fi
done
