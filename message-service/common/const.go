package common

const (
	// APPNAME the name of the service
	APPNAME string = "message-service"

	// VAULTPATH the vault path for configuration
	VAULTPATH string = "/vault/secrets"

	// CENTRIFUGO_SERVICE url
	CENTRIFUGO_SERVICE string = "http://centrifugo:8000/api"
	// CENTRIFUGO_SERVICE string = "https://centrifugo.testing.circleyy.io/api"
)
