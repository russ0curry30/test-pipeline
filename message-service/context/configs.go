package context

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/common"
)

// Config struct for config
type Config struct {
	configs map[string]string
}

// Configuration configuration
var Configuration = Config{
	configs: map[string]string{},
}

// Init loading the configuration
func (config *Config) Init() error {
	os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "/vault/secrets/FCM_key.json")
	config.initAppEnv()
	err := config.loadConfigs()

	return err
}

// Get get value from configs
func (config *Config) Get(key string) string {
	return config.configs[key]
}

func (config *Config) initAppEnv() {
	config.configs["APP_NAME"] = common.APPNAME
	config.configs["ENV"] = os.Getenv("ENV")
	if config.configs["ENV"] == "" {
		config.configs["ENV"] = "local"
	}

	vaultPath := common.VAULTPATH
	if config.configs["ENV"] == "local" {
		vaultPath = "."
	}

	if config.configs["ENV"] == "unittest" {
		vaultPath = ".."
	}

	config.configs["VAULT_PATH"] = vaultPath
}

func (config *Config) loadConfigs() error {
	filepath := fmt.Sprintf("%s/%s_%s.json", config.configs["VAULT_PATH"], config.configs["APP_NAME"], config.configs["ENV"])

	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, &config.configs)
}
