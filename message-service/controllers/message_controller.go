package controllers

import (
	"log"
	"net/http"

	firebase "firebase.google.com/go"
	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/external-apis/centrifugo"
	firebaseFCM "gitlab.com/circleyy_dev/backend/circleyy-services/message-service/external-apis/firebase"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/models"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/services"
)

// MessageController the endpoints for tokens
type MessageController struct {
}

// MessageApis endpoints for updating firebase token
func MessageApis(router *gin.RouterGroup) {
	api := MessageController{}
	router.POST("/publish", api.Publish)
}

// Publish publish message to channels
// @Summary publish message to centrifugo
// @Security securitydefinitions.oauth2.application
// @Tags 2-Message
// @Produce json
// @Param body body dto.PulishMessageRequest true "List of parameters"
// @Success 200 {string} json "{"success":true}"
// @Router /publish [post]
func (MessageController *MessageController) Publish(context *gin.Context) {
	app, ok := context.MustGet("app").(*firebase.App)
	if !ok {
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": "Error getting the app.",
		})
		log.Print("[MessageController.Publish] Error getting the app.")
		return
	}

	request := dto.PulishMessageRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[MessageController.Publish] Error binding json in request body: " + err.Error())
		return
	}

	message := MessageController.newMessage(request)

	statusOK, err := centrifugo.PublishMessage(message)
	if err != nil {
		log.Print("[MessageController.Publish] Error publishing the message: " + err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	if !statusOK {
		log.Print("[MessageController.Publish] Error publishing the message: Response status code is incorrect")
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": "Response status code is incorrect",
		})
		return
	}

	token, err := services.GetTokenByUuid(message.ReceiverId)
	if err != nil {
		log.Print("[MessageController.Publish] Error getting receiver's token: " + err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	err = firebaseFCM.SendNotifToSingleDevice(app, token, message)
	if err != nil {
		log.Print("[MessageController.Publish] Error sending notification to firebase: " + err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{"success": true})
}

func (MessageController *MessageController) newMessage(request dto.PulishMessageRequest) models.Message {
	message := models.Message{
		Id:         request.Id,
		MessageId:  request.MessageId,
		ReceiverId: request.ReceiverId,
		SenderId:   request.SenderId,
		SenderName: request.SenderName,
		CreatedAt:  request.CreatedAt,
		Sent:       request.Sent,
		Received:   request.Received,
		Type:       request.Type,
		Text:       request.Text,
		Image:      request.Image,
		Audio:      request.Audio,
		Video:      request.Video,
		Location:   request.Location,
		Document:   request.Document,
		Sound:      request.Sound,
	}
	return message
}
