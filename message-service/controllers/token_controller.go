package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/dto"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/services"
)

// TokenController the endpoints for tokens
type TokenController struct {
}

// UpdateFirebaseTokenApis  endpoints for updating firebase token
func UpdateFirebaseTokenApis(router *gin.RouterGroup) {
	api := TokenController{}
	router.POST("/firebase-token", api.UpdateToken)
}

// UpdateToken update firebase token for notifications
// @Summary update firebase FCM token
// @Security securitydefinitions.oauth2.application
// @Tags 1-Token
// @Produce json
// @Param body body dto.UpdateTokenRequest true "List of parameters"
// @Success 200 {string} json "{"success":true}"
// @Router /firebase-token [post]
func (TokenController *TokenController) UpdateToken(context *gin.Context) {
	request := dto.UpdateTokenRequest{}
	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[TokenController.UpdateToken] Error binding json in request body: " + err.Error())
		return
	}

	uuid := request.UUID
	token := request.Token

	err = services.UpdateTokenByUuid(uuid, token)
	if err != nil {
		log.Print("[TokenController.UpdateToken] Error updating the token: " + err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{"success": true})
}
