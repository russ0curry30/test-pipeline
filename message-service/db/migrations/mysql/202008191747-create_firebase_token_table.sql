-- +migrate Up
CREATE TABLE IF NOT EXISTS tokens (
    id           INT                AUTO_INCREMENT PRIMARY KEY,
    token        VARCHAR(255)        NOT NULL DEFAULT '',
    uuid         VARCHAR(36)        NOT NULL,
    created_at   DATETIME           DEFAULT CURRENT_TIMESTAMP,
    updated_at   DATETIME           DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (uuid),
    INDEX `index_token` (`uuid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- +migrate Down
DROP TABLE IF EXISTS tokens;