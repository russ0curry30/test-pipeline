package dto

// UpdateTokenRequest request dto
type UpdateTokenRequest struct {
	UUID  string `json:"uuid" binding:"required" example:"2a538125-c4a4-11ea-dc78-0a0d8425ec28"`
	Token string `json:"token" binding:"required" example:"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eI6Ikp.."`
}

type Media struct {
	ExpiresAt    string `json:"expiresAt"`
	ThumbnailURL string `json:"thumbnailURL,omitempty"`
	Url          string `json:"url"`
	MediaID      string `json:"mediaID,omitempty"`
	LocalFileURL string `json:"localFileURL,omitempty"`
}

// Publish message request dto
type PulishMessageRequest struct {
	Id         string `json:"id"`
	MessageId  string `json:"messageId"`
	ReceiverId string `json:"receiverId" binding:"required"`
	SenderId   string `json:"senderId" binding:"required"`
	SenderName string `json:"senderName" binding:"required"`
	CreatedAt  string `json:"createdAt"`
	Sent       bool   `json:"sent"`
	Received   bool   `json:"received"`
	Type       string `json:"type" binding:"required"`
	Text       string `json:"text",omitempty`
	Image      Media  `json:"image",omitempty`
	Audio      string `json:"audio",omitempty`
	Video      string `json:"video",omitempty`
	Location   string `json:"location",omitempty`
	Document   string `json:"document",omitempty`
	Sound      string `json:"sound",omitempty`
}
