package centrifugo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"text/template"

	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/common"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/models"
)

type Data struct {
	Channel string
	Payload map[string]interface{}
}

func getFormattedPayload(channelID string, message models.Message) string {

	var payload map[string]interface{}
	inrec, _ := json.Marshal(&message)
	json.Unmarshal(inrec, &payload)

	data := Data{
		Channel: channelID,
		Payload: payload,
	}

	const payloadTmpl = `{
			"method": "publish",
			"params": {
				"channel": "{{ .Channel }}", 
				"data": {
					"receiverId" : "{{ .Payload.receiverId }}",
					"senderId" : "{{ .Payload.senderId }}",
					{{ if .Payload.createdAt }}	"createdAt" : "{{ .Payload.createdAt }}", {{ end }}
					{{ if .Payload.id }}	"id" : "{{ .Payload.id }}", {{ end }}
					{{ if .Payload.messageId }}	"messageId" : "{{ .Payload.messageId }}", {{ end }}
					{{ if .Payload.received }}	"received" : "{{ .Payload.received }}", {{ end }}
					{{ if .Payload.sent }}	"sent" : "{{ .Payload.sent }}", {{ end }}
					{{ if eq .Payload.type  "document" }}	"document" : "{{ .Payload.document }}", {{ end }}
					{{ if eq .Payload.type  "location" }}	"location" : "{{ .Payload.location }}", {{ end }}
					{{ if eq .Payload.type  "sound" }}	"sound" : "{{ .Payload.sound}}", {{ end }}
					{{ if eq .Payload.type  "text" }}	"text" : "{{ .Payload.text }}", {{ end }}
					{{ if eq .Payload.type  "video" }}	"video" : "{{ .Payload.video }}", {{ end }}
					{{ if eq .Payload.type  "audio" }}	"audio" : "{{ .Payload.audio }}", {{ end }}
					{{ if eq .Payload.type  "image" }}  "image": { 
						"expiresAt": "{{ .Payload.image.expiresAt }}",
						{{ if .Payload.image.mediaID }}	"mediaID": "{{ .Payload.image.mediaID }}", {{ end }}
						{{ if .Payload.image.thumbnailURL }}	"thumbnailURL": "{{ .Payload.image.thumbnailURL }}", {{ end }}
						"url":  "{{ .Payload.image.url }}"
					}, {{end}}
					"type" : "{{ .Payload.type }}"
				}
			}
		}`
	t := template.Must(template.New("payload").Parse(payloadTmpl))
	buf := &bytes.Buffer{}
	err := t.Execute(buf, data)
	if err != nil {
		panic(err)
		return ""
	}

	str := buf.String()
	return str
}

func PublishMessage(message models.Message) (bool, error) {
	apiKey := context.Configuration.Get("CENTRIFUGO_API_KEY")
	senderPayload := getFormattedPayload(message.SenderId, message)
	receiverPayload := getFormattedPayload(message.ReceiverId, message)

	req, err := http.NewRequest("POST", common.CENTRIFUGO_SERVICE, bytes.NewBuffer([]byte(receiverPayload)))
	if err != nil {
		log.Println("[Message Error] " + err.Error())
	}

	req2, err := http.NewRequest("POST", common.CENTRIFUGO_SERVICE, bytes.NewBuffer([]byte(senderPayload)))
	if err != nil {
		log.Println("[Message Error] " + err.Error())
	}

	req.Header.Add("Authorization", apiKey)
	req2.Header.Add("Authorization", apiKey)
	req.Header.Add("Content-Type", "application/json")
	req2.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("[Message Error] Error publishing message " + err.Error())
		return false, err
	}
	statusOK := res.StatusCode >= 200 && res.StatusCode < 300
	if !statusOK {
		status := fmt.Sprintf("%d", res.StatusCode)
		log.Println("[Message Error] Error publishing message, got statusCode " + status)
		return false, nil
	}

	res2, err := client.Do(req2)
	if err != nil {
		log.Println("[Message Error] Error publishing message " + err.Error())
		return false, err
	}

	statusOK = res2.StatusCode >= 200 && res2.StatusCode < 300
	if !statusOK {
		status := fmt.Sprintf("%d", res2.StatusCode)
		log.Println("[Message Error] Error publishing message, got statusCode " + status)
		return false, nil
	}

	return true, nil
}
