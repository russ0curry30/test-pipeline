package firebase

import (
	"context"
	"fmt"
	"log"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/models"
)

func formatMessage(message models.Message) map[string]string {
	var msg map[string]string
	if message.Type == "image" {
		msg = map[string]string{
			"type":         "image",
			"senderId":     message.SenderId,
			"receiverId":   message.ReceiverId,
			"senderName":   message.SenderName,
			"url":          message.Image.Url,
			"thumbnailURL": message.Image.ThumbnailURL,
			"text":         "[Photo]"}
	}

	if message.Type == "text" {
		msg = map[string]string{
			"type":       "text",
			"senderId":   message.SenderId,
			"receiverId": message.ReceiverId,
			"senderName": message.SenderName,
			"text":       message.Text,
		}
	}
	return msg
}

func SendNotifToSingleDevice(app *firebase.App, token string, message models.Message) error {
	ctx := context.Background()
	// Obtain a messaging.Client from the App.
	client, err := app.Messaging(ctx)
	if err != nil {
		log.Fatalf("error getting Messaging client: %v\n", err)
	}

	formattedMsg := formatMessage(message)

	fbMsg := &messaging.Message{
		Data: formattedMsg,
		Notification: &messaging.Notification{
			Title: formattedMsg["senderName"],
			Body:  formattedMsg["text"],
		},
		Token: token, // This token comes from the client FCM SDKs.
	}

	// Send a message to the device corresponding to the provided registration token.
	response, err := client.Send(ctx, fbMsg)
	if err != nil {
		log.Fatalln(err)
		return err
	}

	// Response is a message ID string.
	fmt.Println("Successfully sent message:", response)
	return nil

}
