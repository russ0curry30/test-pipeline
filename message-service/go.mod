module gitlab.com/circleyy_dev/backend/circleyy-services/message-service

go 1.14

require (
	cloud.google.com/go v0.64.0 // indirect
	cloud.google.com/go/firestore v1.3.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	firebase.google.com/go/v4 v4.0.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/googleapis/gax-go v1.0.3 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.7
	golang.org/x/exp v0.0.0-20200819202907-27b6b2ade93b // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc
	golang.org/x/sys v0.0.0-20200819171115-d785dc25833f // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/tools v0.0.0-20200820010801-b793a1359eac // indirect
	google.golang.org/api v0.30.0
	google.golang.org/genproto v0.0.0-20200815001618-f69a88009b70 // indirect
	google.golang.org/grpc v1.31.0 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	honnef.co/go/tools v0.0.1-2020.1.5 // indirect
)
