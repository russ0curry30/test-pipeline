package main

import (
	"log"

	"context"

	firebase "firebase.google.com/go"
	appContext "gitlab.com/circleyy_dev/backend/circleyy-services/message-service/context"
	route "gitlab.com/circleyy_dev/backend/circleyy-services/message-service/routes"
)

// @title Message Service APIs
// @version 1.0
// @description This is API Documentation for Message Service

// @termsOfService http://swagger.io/terms/

// @contact.name CircleYY
// @contact.url http://www.circleyy.io/
// @contact.email support@circleyy.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /message/v1

func main() {

	err := appContext.Configuration.Init()

	if err != nil {
		log.Fatal("Error when init configs:", err)
	}

	appContext.DB.Migrate()

	// Init a firebase app
	ctx := context.Background()
	app, err := firebase.NewApp(ctx, nil)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

	router := route.InitRoutes(app)

	log.Println("Starting the server...")
	router.Run(":8080")
}
