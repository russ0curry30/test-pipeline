package models

import (
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/dto"
)

// Message model
type Message struct {
	Id         string    `json:"id,omitempty"`
	MessageId  string    `json:"messageId,omitempty"`
	ReceiverId string    `json:"receiverId"`
	SenderId   string    `json:"senderId"`
	SenderName string    `json:"senderName"`
	CreatedAt  string    `json:"createdAt"`
	Sent       bool      `json:"sent,omitempty"`
	Received   bool      `json:"received,omitempty"`
	Type       string    `json:"type,omitempty"`
	Text       string    `json:"text,omitempty"`
	Image      dto.Media `json:"image,omitempty"`
	Audio      string    `json:"audio,omitempty"`
	Video      string    `json:"video,omitempty"`
	Location   string    `json:"location,omitempty"`
	Document   string    `json:"document,omitempty"`
	Sound      string    `json:"sound,omitempty"`
}
