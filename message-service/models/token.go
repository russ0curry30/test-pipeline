package models

import (
	"time"
)

// Token model
type Token struct {
	ID        int       `db:"id" json:"-"`
	Token     string    `db:"token" json:"token"`
	UUID      string    `db:"uuid" json:"uuid"`
	CreatedAt time.Time `db:"created_at" json:"-"`
	UpdateAt  time.Time `db:"updated_at" json:"-"`
	ExpireAt  time.Time `db:"expire_at" json:"-"`
}
