package route

import (
	"fmt"
	"time"

	firebase "firebase.google.com/go"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/controllers"
	_ "gitlab.com/circleyy_dev/backend/circleyy-services/message-service/docs"
)

// InitRoutes setup the routers
func InitRoutes(app *firebase.App) *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())
	router.Use(func(context *gin.Context) {
		context.Set("app", app)
		context.Next()
	})

	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))
	router.Use(gin.Recovery())

	swagger := router.Group("/message")
	{
		swagger.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	apiGroup := router.Group("/message/v1")
	controllers.UpdateFirebaseTokenApis(apiGroup)
	controllers.MessageApis(apiGroup)

	return router
}
