package services

import (
	"gitlab.com/circleyy_dev/backend/circleyy-services/message-service/context"
)

func UpdateTokenByUuid(uuid, token string) error {
	q := `INSERT INTO tokens(uuid, token) VALUES(?, ?) ON DUPLICATE KEY UPDATE uuid=?, token=?`

	dbConn := context.DB.GetDBConnection()

	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return err
	}

	_, err = insertStmt.Exec(uuid, token, uuid, token)
	if err != nil {
		return err
	}

	return nil
}

func GetTokenByUuid(uuid string) (string, error) {
	dbConn := context.DB.GetDBConnection()
	q := `SELECT token from tokens where uuid=?`

	stmt, err := dbConn.Prepare(q)
	if err != nil {
		return "", err
	}

	var token string
	err = stmt.QueryRow(uuid).Scan(&token)

	return token, err
}
