package common

const (
	// APPNAME the name of the service
	APPNAME string = "storage-service"

	// VAULTPATH the vault path for configuration
	VAULTPATH string = "/vault/secrets"
)

// UploadURLExpirationInMinutes expires in 15 minutes (the maximum expiration time is one week/10080 minutes)
const UploadURLExpirationInMinutes = 15

// GetURLExpirationInMinutes expires in 72 hours
const GetURLExpirationInMinutes = 72 * 60
