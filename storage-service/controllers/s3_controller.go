package controllers

import (
	"log"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gin-gonic/gin"
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/common"
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/dto"

	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/services"
)

// PresignedURLController the endpoints for users
type PresignedURLController struct {
}

// PresignedURLApis register user apis to router
func PresignedURLApis(router *gin.RouterGroup) {
	api := PresignedURLController{}
	router.POST("/get-url", api.GetURL)
	router.POST("/upload-url", api.UploadURL)
}

// GetURL Get s3 pre-signed url
// @Produce json
// @Security securitydefinitions.oauth2.application
// @description Get pre-signed url to view the object in s3 buckets
// @Param body body dto.GetURLRequest true "List of parameters"
// @Success 200 {object} dto.GetURLResponse
// @Router /get-url [post]
func (PresignedURLController *PresignedURLController) GetURL(context *gin.Context) {
	request := dto.GetURLRequest{}
	sess, ok := context.MustGet("sess").(*session.Session)
	if !ok {
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": "Error getting the session",
		})
		log.Print("[PresignedURLController.GetURL] Error getting the session.")
		return
	}

	err := context.BindJSON(&request)
	if err != nil {
		log.Print("[PresignedURLController.GetURL] Error binding json in request body: " + err.Error())
		return
	}

	bucket := request.Bucket
	key := request.Key

	awsService := services.AWSService{}
	url, thumbnailURL, err := awsService.GetSignedURL(bucket, key, sess, common.GetURLExpirationInMinutes*time.Minute, "get")

	if err != nil {
		log.Print("[PresignedURLController.GetURL] Error getting presigned url: " + err.Error())
		return
	}

	expiresAtTimestamp := time.Now().Unix() + int64(common.UploadURLExpirationInMinutes)*60
	expiresAt := time.Unix(expiresAtTimestamp, 0).Format(time.RFC3339)

	context.JSON(http.StatusOK, gin.H{
		"url":          url,
		"thumbnailUrl": thumbnailURL,
		"expiresAt":    expiresAt,
	})
}

// UploadURL Get s3 pre-signed url to upload
// @Produce json
// @security securitydefinitions.oauth2.application OAuth2Application
// @description Get pre-signed url to upload the object to s3 buckets
// @Param body body dto.UploadURLRequest true "List of parameters"
// @Success 200 {object} dto.UploadURLResponse
// @Router /upload-url [post]
func (PresignedURLController *PresignedURLController) UploadURL(context *gin.Context) {
	request := dto.UploadURLRequest{}
	sess, ok := context.MustGet("sess").(*session.Session)
	err := context.BindJSON(&request)
	if !ok {
		log.Print("[PresignedURLController.UploadURL] Error getting the session.")
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": "Cannot get a session from AWS",
		})
		return
	}

	if err != nil {
		log.Print("[PresignedURLController.UploadURL] Error binding json in request body: " + err.Error())
		return
	}

	bucket := request.Bucket
	key := request.Key

	awsService := services.AWSService{}
	uploadURL, uploadThumbnailURL, err := awsService.GetSignedURL(bucket, key, sess, common.UploadURLExpirationInMinutes*time.Minute, "put")
	url, thumbnailURL, err := awsService.GetSignedURL(bucket, key, sess, common.GetURLExpirationInMinutes*time.Minute, "get")
	if err != nil {
		log.Print("[PresignedURLController.UploadURL] Error getting presigned url: " + err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	// urlModel := models.URL{
	// 	Bucket:    bucket,
	// 	ObjectKey: key,
	// }

	// urlID, err := services.SaveURL(&urlModel)
	// _ = urlID

	// if err != nil {
	// 	log.Print("[PresignedURLController.UploadURL] Error saving to the db: " + err.Error())
	// 	context.JSON(http.StatusInternalServerError, gin.H{
	// 		"error": err.Error(),
	// 	})
	// 	return
	// }

	expiresAtTimestamp := time.Now().Unix() + int64(common.UploadURLExpirationInMinutes)*60
	expiresAt := time.Unix(expiresAtTimestamp, 0).Format(time.RFC3339)

	context.JSON(http.StatusOK, gin.H{
		"uploadUrl":          uploadURL,
		"uploadThumbnailUrl": uploadThumbnailURL,
		"url":                url,
		"thumbnailUrl":       thumbnailURL,
		"expiresAt":          expiresAt,
	})
}
