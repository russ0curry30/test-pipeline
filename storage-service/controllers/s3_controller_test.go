package controllers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/services"
)

func initTestRouter(sess *session.Session, res *httptest.ResponseRecorder) *gin.Engine {

	gin.SetMode(gin.TestMode)

	_, router := gin.CreateTestContext(res)
	router.Use(cors.Default())
	router.Use(func(context *gin.Context) {
		context.Set("sess", sess)
		context.Next()
	})

	return router
}

func performRequest(router http.Handler, method, path string, payload map[string]string) *httptest.ResponseRecorder {
	res := httptest.NewRecorder()

	if payload != nil {
		jsonValue, _ := json.Marshal(payload)
		req, _ := http.NewRequest(method, path, bytes.NewBuffer(jsonValue))
		router.ServeHTTP(res, req)
		return res
	}

	req, _ := http.NewRequest(method, path, nil)
	router.ServeHTTP(res, req)
	return res
}

func TestUrlEndpoints(t *testing.T) {
	err := context.Configuration.Init()
	if err != nil {
		log.Fatal("Error when init configs:", err)
	}

	res := httptest.NewRecorder()
	awsService := services.AWSService{}
	sess, _ := awsService.ConnectAWS()

	router := initTestRouter(sess, res)
	urlAPIGroup := router.Group("/storage/v1")
	PresignedURLApis(urlAPIGroup)

	// Bad request 400, Error binding json
	payload := map[string]string{"bucketsss": "x", "key": "x"}
	res = performRequest(router, "POST", "/storage/v1/get-url", payload)
	if res.Code != 400 {
		t.Errorf("Error Code was incorrect, got: %d, want: %d.", res.Code, 400)
	}
	res = performRequest(router, "POST", "/storage/v1/upload-url", payload)
	if res.Code != 400 {
		t.Errorf("Error Code was incorrect, got: %d, want: %d.", res.Code, 400)
	}

	// Good request 200
	payload = map[string]string{"bucket": "good", "key": "request"}

	// get-url
	res = performRequest(router, "POST", "/storage/v1/get-url", payload)
	if res.Code != 200 {
		t.Errorf("Error Code was incorrect, got: %d, want: %d.", res.Code, 200)
	}

	var response map[string]string
	_ = json.Unmarshal([]byte(res.Body.String()), &response)
	_, urlExists := response["url"]
	_, thumbnailURLExists := response["thumbnailUrl"]
	_, expiresAtExists := response["expiresAt"]

	if !urlExists || !thumbnailURLExists || !expiresAtExists {
		t.Errorf("Error response data missing, got: %+v, want: {url: xx, thumbnailUrl:xx, expiresAt:xx }", response)
	}

	//  upload-url
	res = performRequest(router, "POST", "/storage/v1/upload-url", payload)
	if res.Code != 200 {
		t.Errorf("Error Code was incorrect, got: %d, want: %d.", res.Code, 200)
	}

	_ = json.Unmarshal([]byte(res.Body.String()), &response)
	_, urlExists = response["url"]
	_, thumbnailURLExists = response["thumbnailUrl"]
	_, uploadURLExists := response["uploadUrl"]
	_, uploadThumbnailURLExists := response["uploadThumbnailUrl"]
	_, expiresAtExists = response["expiresAt"]

	if !urlExists || !thumbnailURLExists || !expiresAtExists || !uploadURLExists || !uploadThumbnailURLExists {
		t.Errorf("Error response data missing, got: %+v, want: {url: xx, thumbnailUrl:xx, uploadUrl:xx, uploadThumbnailUrl:xx, expiresAt:xx }", response)
	}

}
