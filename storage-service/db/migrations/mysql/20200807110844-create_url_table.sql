-- +migrate Up
CREATE TABLE IF NOT EXISTS urls (
    id           INT                AUTO_INCREMENT PRIMARY KEY,
    bucket       VARCHAR(255)       NOT NULL,
    object_key   VARCHAR(255)       NOT NULL,
    created_at  DATETIME            DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME            DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at  DATETIME            NULL,
    UNIQUE (`object_key`),
    INDEX `index_url` (`object_key`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


-- +migrate Down
DROP TABLE IF EXISTS urls;
