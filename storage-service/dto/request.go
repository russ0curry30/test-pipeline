package dto

// GetURLRequest request dto
type GetURLRequest struct {
	Bucket string `json:"bucket" binding:"required" example:"circleyy-app-images"`
	Key    string `json:"key" binding:"required" example:"img-name.png"`
}

// UploadURLRequest request dto
type UploadURLRequest struct {
	Bucket string `json:"bucket" binding:"required" example:"circleyy-app-images"`
	Key    string `json:"key" binding:"required" example:"img-name.png"`
}
