package dto

import "time"

// GetURLResponse response dto for URLs
type GetURLResponse struct {
	URL          string    `json:"url" example:"https://circleyy-xxx-amazonaws.com/image.png?&X-Amz-Algorithm=xxxxx"`
	ThumbnailURL string    `json:"thumbnailUrl" example:"https://circleyy-xxx-amazonaws.com/image.png?&X-Amz-Algorithm=xxxxx"`
	ExpiresAt    time.Time `json:"expiresAt" example:"2020-08-12T18:29:04+10:00"`
}

// UploadURLResponse response dto for URLs
type UploadURLResponse struct {
	UploadURL          string    `json:"uploadUrl" example:"https://circleyy-xxx-amazonaws.com/image.png?&X-Amz-Algorithm=xxxxx"`
	UploadThumbnailURL string    `json:"uploadThumbnailUrl" example:"https://circleyy-xxx-amazonaws.com/image.png?&X-Amz-Algorithm=xxxxx"`
	URL                string    `json:"url" example:"https://circleyy-xxx-amazonaws.com/image.png?&X-Amz-Algorithm=xxxxx"`
	ThumbnailURL       string    `json:"thumbnailUrl" example:"https://circleyy-xxx-amazonaws.com/image.png?&X-Amz-Algorithm=xxxxx"`
	ExpiresAt          time.Time `json:"expiresAt" example:"2020-08-12T18:29:04+10:00"`
}
