package main

import (
	"log"

	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/context"
	route "gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/routes"
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/services"
)

// @title Storage Service APIs
// @version 1.0
// @description This is API Documentation for Storage Service

// @termsOfService http://swagger.io/terms/

// @contact.name CircleYY
// @contact.url http://www.circleyy.io/
// @contact.email support@circleyy.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /storage/v1

func main() {

	err := context.Configuration.Init()

	if err != nil {
		log.Fatal("Error when init configs:", err)
	}

	context.DB.Migrate()

	awsService := services.AWSService{}
	sess, err := awsService.ConnectAWS()

	if err != nil {
		log.Println("Failed to connect to AWS!")
	}

	router := route.InitRoutes(sess)

	log.Println("Starting the server...")
	router.Run(":8080")
}
