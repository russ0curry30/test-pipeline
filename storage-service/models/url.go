package models

import (
	"time"
)

// URL model
type URL struct {
	ID        int64     `db:"id" json:"-"`
	Bucket    string    `db:"bucket" json:"-"`
	ObjectKey string    `db:"object_key" json:"-"`
	CreatedAt time.Time `db:"created_at" json:"-"`
	UpdatedAt time.Time `db:"updated_at" json:"-"`
	DeletedAt time.Time `db:"deleted_at" json:"-"`
}
