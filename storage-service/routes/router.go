package route

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/controllers"
	_ "gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/docs"
)

// InitRoutes setup the routers
func InitRoutes(sess *session.Session) *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())
	router.Use(func(context *gin.Context) {
		context.Set("sess", sess)
		context.Next()
	})
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))
	router.Use(gin.Recovery())

	swagger := router.Group("/storage")
	{
		swagger.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	urlAPIGroup := router.Group("/storage/v1")
	controllers.PresignedURLApis(urlAPIGroup)

	return router
}
