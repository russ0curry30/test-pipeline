package route

import (
	"testing"

	"github.com/aws/aws-sdk-go/awstesting/mock"
)

func TestInitRouters(t *testing.T) {
	router := InitRoutes(mock.Session)

	group := router.Group("/storage/v1")
	if group.BasePath() != "/storage/v1" {
		t.Errorf("Test failed on router group")
	}
}
