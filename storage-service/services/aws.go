package services

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/context"
)

// AWSService AWSService
type AWSService struct {
}

// ConnectAWS Create a session
func (AWSService *AWSService) ConnectAWS() (*session.Session, error) {
	AccessKey := context.Configuration.Get("AWS_ACCESS_KEY_ID")
	SecretKey := context.Configuration.Get("AWS_SECRET_ACCESS_KEY")
	Region := context.Configuration.Get("AWS_DEFAULT_REGION")
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(Region),
		Credentials: credentials.NewStaticCredentials(AccessKey, SecretKey, ""),
	})
	return sess, err
}

// GetSignedURL Return a pre-signed url
func (AWSService *AWSService) GetSignedURL(bucket string, objectKey string, sess *session.Session, ttl time.Duration, purpose string) (string, string, error) {
	// Create S3 service client
	svc := s3.New(sess)
	if purpose == "put" {
		req, _ := svc.PutObjectRequest(&s3.PutObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(objectKey),
		})

		req2, _ := svc.PutObjectRequest(&s3.PutObjectInput{
			Bucket: aws.String(bucket + "-resized"),
			Key:    aws.String("resized-" + objectKey),
		})

		url, err := req.Presign(ttl)
		thumbnailURL, err := req2.Presign(ttl)

		return url, thumbnailURL, err
	}

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(objectKey),
	})

	req2, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(bucket + "-resized"),
		Key:    aws.String("resized-" + objectKey),
	})

	url, err := req.Presign(ttl)
	thumbnailURL, err := req2.Presign(ttl)

	return url, thumbnailURL, err
}
