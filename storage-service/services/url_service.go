package services

import (
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/context"
	"gitlab.com/circleyy_dev/backend/circleyy-services/storage-service/models"
)

// SaveURL save url to the database
func SaveURL(url *models.URL) (int64, error) {
	dbConn := context.DB.GetDBConnection()

	q := "INSERT INTO urls ( bucket, object_key )" +
		"VALUES ( ?, ?)"
	insertStmt, err := dbConn.Prepare(q)
	if err != nil {
		return 1, err
	}
	result, err := insertStmt.Exec(url.Bucket, url.ObjectKey)
	if err != nil {
		return 1, err
	}
	urlID, err := result.LastInsertId()

	return urlID, nil
}
